<?php
/**
 * Copyright © Resurs Bank AB. All rights reserved.
 * See LICENSE for license details.
 */

declare(strict_types=1);

namespace Resursbank\Core\Model;

/**
 * Resurs Bank order states
 */
class OrderStates
{
    public const PAYMENT_PENDING = 'PS_RB_OS_PAYMENT_PENDING';

    /**
     * @return array[]
     */
    public static function states(): array
    {
        return [
             self::PAYMENT_PENDING => [
                 'name' => 'Resurs Bank - Payment Pending',
                 'send_email' => 0,
                 'invoice' => 0,
                 'color' => '#c85f00',
                 'unremovable' => 1,
                 'hidden' => 1,
                 'logable' => 0,
                 'paid' => 0,
             ],
        ];
    }
}
