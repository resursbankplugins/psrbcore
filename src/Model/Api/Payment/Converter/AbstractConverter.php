<?php
/**
 * Copyright © Resurs Bank AB. All rights reserved.
 * See LICENSE for license details.
 */

declare(strict_types=1);

namespace Resursbank\Core\Model\Api\Payment\Converter;

use Exception;
use Resursbank\Core\Logger\Logger;
use Resursbank\Core\Model\Api\Payment\Converter\Item\DiscountItem;
use Resursbank\Core\Model\Api\Payment\Converter\Item\ShippingItem;
use Resursbank\Core\Model\Api\Payment\Item as PaymentItem;

/**
 * Basic data conversion class for payment payload.
 */
abstract class AbstractConverter implements ConverterInterface
{
    /**
     * @var Logger
     */
    private $log;

    /**
     * @param Logger $log
     */
    public function __construct(
        Logger $log
    ) {
        $this->log = $log;
    }

    /**
     * {@inheritdoc}
     *
     * @throws Exception
     */
    public function getShippingData(
        string $method,
        string $description,
        float $amount,
        float $vatPct
    ): array {
        $result = [];

        if ($this->includeShippingData($method, $amount)) {
            $item = new ShippingItem(
                $method,
                $description,
                $amount,
                (int) $vatPct
            );

            $result[] = $item->getItem();
        }

        return $result;
    }

    /**
     * {@inheritdoc}
     *
     * @throws Exception
     */
    public function getDiscountData(
        string $couponCode,
        float $amount,
        float $taxAmount
    ): ?PaymentItem {
        if ($this->includeDiscountData($amount)) {
            $item = new DiscountItem(
                $couponCode,
                -$amount,
                $taxAmount
            );

            return $item->getItem();
        }

        return null;
    }

    /**
     * {@inheritdoc}
     */
    public function includeShippingData(
        string $method,
        float $amount
    ): bool {
        return $method !== '' && $amount > 0;
    }

    /**
     * {@inheritdoc}
     */
    public function includeDiscountData(
        float $amount
    ): bool {
        return $amount > 0;
    }

    /**
     * Convert all PaymentItem instances to simple arrays the API can
     * understand.
     *
     * @param PaymentItem[] $items
     *
     * @return array<array>
     */
    public function convertItemsToArrays(
        array $items
    ): array {
        $result = [];

        foreach ($items as $item) {
            $result[] = $item->toArray();
        }

        return $result;
    }

    /**
     * Retrieve applied tax percentage from order entity by type (product,
     * shipping etc.).
     *
     * @param int $orderId
     * @param string $type
     *
     * @return float
     */
    public function getTaxPercentage(
        int $orderId,
        string $type
    ): float {
        $result = 0.0;

        // TODO: Can't retrieve tax_rate from carrier. Have to calculate yourself
//        $taxItem = $this->taxResourceFactory->create();
//        $collection = $taxItem->getTaxItemsByOrderId($orderId);
//
//        $match = false;
//
//        /** @var array<mixed> $item */
//        foreach ($collection as $item) {
//            if (is_array($item) &&
//                isset($item['taxable_item_type']) &&
//                $item['taxable_item_type'] === $type
//            ) {
//                $match = true;
//
//                $result = isset($item['tax_percent']) ?
//                    (float) $item['tax_percent'] :
//                    0.0;
//
//                break;
//            }
//        }
//
//        if (!$match) {
//            $this->log->info(
//                'Could not find matching tax item type ' . $type . ' on ' .
//                'order entity ' . $orderId
//            );
//        }

        return $result;
    }
}
