<?php
/**
 * Copyright © Resurs Bank AB. All rights reserved.
 * See LICENSE for license details.
 */

declare(strict_types=1);

namespace Resursbank\Core\Model\Api\Payment\Converter\Item;

use Exception;
use Resursbank\Core\Model\Api\Payment\Item;
use Resursbank\Core\Model\Api\Payment\Item\Validation\ArtNo;
use Resursbank\Core\Model\Api\Payment\Item\Validation\UnitAmountWithoutVat;
use function strlen;

/**
 * Convert an item entity, such as an Order Item, into an object prepared for a
 * payment payload.
 */
abstract class AbstractItem implements ItemInterface
{
    /**
     * @return Item
     * @throws Exception
     */
    public function getItem(): Item
    {
        return new Item(
            $this->getArtNo(),
            $this->getDescription(),
            $this->getQuantity(),
            $this->getUnitMeasure(),
            $this->getUnitAmountWithoutVat(),
            $this->getVatPct(),
            $this->getType()
        );
    }

    /**
     * Unit measurement configuration value.
     *
     * @return string
     */
    public function getUnitMeasure(): string
    {
        return Item::UNIT_MEASURE;
    }

    /**
     * Whether to round tax percentage values.
     *
     * NOTE: This currently only applies to payment lines with type DISCOUNT,
     * or ORDER_LINE lines including payment fee information.
     *
     * @return bool
     * @throws Exception
     */
    public function roundTaxPercentage(): bool
    {
        return true;
    }

    /**
     * Removes all illegal characters for the "artNo" property. String length
     * may not exceed 100 characters. Please refer to the linked documentation
     * for further information.
     *
     * @param string $artNo
     * @return string
     * @link https://test.resurs.com/docs/display/ecom/Hosted+payment+flow+data
     */
    public function sanitizeArtNo(
        string $artNo
    ): string {
        $result = (string) preg_replace(ArtNo::REGEX, '', strtolower($artNo));

        if (strlen($result) > ArtNo::MAX_LENGTH) {
            $result = substr($result, 0, ArtNo::MAX_LENGTH);
        }

        return $result;
    }

    /**
     * The "unitAmountWithoutVat" property may not include more than 5 decimals.
     * Please refer to the linked documentation for further information.
     *
     * @param float $amount
     * @return float
     * @link https://test.resurs.com/docs/display/ecom/Hosted+payment+flow+data
     */
    public function sanitizeUnitAmountWithoutVat(
        float $amount
    ): float {
        return round($amount, UnitAmountWithoutVat::MAX_DECIMAL_LENGTH);
    }
}
