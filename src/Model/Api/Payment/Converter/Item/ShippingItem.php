<?php
/**
 * Copyright © Resurs Bank AB. All rights reserved.
 * See LICENSE for license details.
 */

declare(strict_types=1);

namespace Resursbank\Core\Model\Api\Payment\Converter\Item;

use Resursbank\Core\Model\Api\Payment\Item;

/**
 * Shipping data converter.
 */
class ShippingItem extends AbstractItem
{
    /**
     * @var string
     */
    private $method;

    /**
     * @var string
     */
    private $description;

    /**
     * @var float
     */
    private $amount;

    /**
     * @var int
     */
    private $vatPct;

    /**
     * @param string $method shipping method code
     * @param string $description shipping method title
     * @param float $amount Amount incl. tax.
     * @param int $vatPct tax percentage
     */
    public function __construct(
        string $method,
        string $description,
        float $amount,
        int $vatPct
    ) {
        $this->method = $method;
        $this->description = $description;
        $this->amount = $amount;
        $this->vatPct = $vatPct;
    }

    /**
     * {@inheritdoc}
     */
    public function getArtNo(): string
    {
        return $this->sanitizeArtNo($this->method);
    }

    /**
     * {@inheritdoc}
     */
    public function getDescription(): string
    {
        return $this->description;
    }

    /**
     * {@inheritdoc}
     */
    public function getQuantity(): float
    {
        return 1.0;
    }

    /**
     * {@inheritdoc}
     */
    public function getUnitAmountWithoutVat(): float
    {
        $inclTax = $this->amount;
        $vatPct = $this->getVatPct();

        $result = ($inclTax > 0 && $vatPct > 0) ?
            $inclTax / (1 + ($vatPct / 100)) :
            $inclTax;

        return $this->sanitizeUnitAmountWithoutVat($result);
    }

    /**
     * {@inheritdoc}
     */
    public function getVatPct(): int
    {
        return $this->vatPct;
    }

    /**
     * {@inheritdoc}
     */
    public function getType(): string
    {
        return Item::TYPE_SHIPPING;
    }
}
