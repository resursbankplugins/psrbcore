<?php
/**
 * Copyright © Resurs Bank AB. All rights reserved.
 * See LICENSE for license details.
 */

declare(strict_types=1);

namespace Resursbank\Core\Model\Api\Payment\Converter\Item;

use PrestaShop\PrestaShop\Adapter\Entity\OrderDetail;
use Resursbank\Core\Model\Api\Payment\Item;

/**
 * Product data converter.
 */
class ProductItem extends AbstractItem
{
    /**
     * @var OrderDetail
     */
    protected $orderDetail;

    public function __construct(
        OrderDetail $orderDetail
    ) {
        $this->orderDetail = $orderDetail;
    }

    /**
     * {@inheritdoc}
     */
    public function getArtNo(): string
    {
        // default to merchant defined product reference
        $artNo = $this->orderDetail->product_reference;

        // but if it is empty use product_id
        if(empty($artNo)) {
           $artNo = "id" . $this->orderDetail->product_id;
           // and prefix with variation id if applicable
           if(!empty($this->orderDetail->product_attribute_id)) {
               $artNo .= 'v' . $this->orderDetail->product_attribute_id;
           }
        }

        return $this->sanitizeArtNo($artNo);
    }

    /**
     * {@inheritdoc}
     */
    public function getDescription(): string
    {
        return $this->orderDetail->product_name;
    }

    /**
     * {@inheritdoc}
     */
    public function getQuantity(): float
    {
        return (float) $this->orderDetail->product_quantity;
    }

    /**
     * {@inheritdoc}
     */
    public function getUnitAmountWithoutVat(): float
    {
        return $this->sanitizeUnitAmountWithoutVat(
            (float) $this->orderDetail->unit_price_tax_excl
        );
    }

    /**
     * {@inheritdoc}
     */
    public function getVatPct(): int
    {
        // There is no reliable way to get the VAT percentage, so we have
        // to calculate it ourselves
        $priceExclVat = $this->orderDetail->unit_price_tax_excl;
        $priceInclVat = $this->orderDetail->unit_price_tax_incl;
        $vatPct = ($priceInclVat - $priceExclVat) / $priceExclVat * 100;

        return (int) round($vatPct);
    }

    /**
     * {@inheritdoc}
     */
    public function getType(): string
    {
        return Item::TYPE_PRODUCT;
    }
}
