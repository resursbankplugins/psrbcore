<?php
/**
 * Copyright © Resurs Bank AB. All rights reserved.
 * See LICENSE for license details.
 */
/* @noinspection PhpCastIsUnnecessaryInspection */

declare(strict_types=1);

namespace Resursbank\Core\Model\Api\Payment\Converter;

use Exception;
use Order;
use PrestaShop\PrestaShop\Adapter\Entity\Carrier;
use PrestaShop\PrestaShop\Adapter\Entity\OrderCartRule;
use PrestaShop\PrestaShop\Adapter\Entity\OrderDetail;
use Resursbank\Core\Model\Api\Payment\Converter\Item\ProductItem;
use Resursbank\Core\Model\Api\Payment\Item as PaymentItem;

/**
 * Quote entity conversion for payment payloads.
 */
class OrderConverter extends AbstractConverter
{
    /**
     * Convert supplied entity to a collection of PaymentItem instances. These
     * objects can later be mutated into a simple array the API can interpret.
     *
     * @param Order $order
     *
     * @return PaymentItem[]
     *
     * @throws Exception
     */
    public function convert(
        Order $order
    ): array {
        $carrier = new Carrier($order->id_carrier, $order->id_lang);
        $orderCartRules = $order->getCartRules();
        $discountData = [];
        /** @var OrderCartRule $orderCartRule */
        foreach ($orderCartRules as $orderCartRule) {
            $discountData[] = $this->getDiscountData(
                (string) $orderCartRule['name'],
                (float) $orderCartRule['value'],
                (float) $orderCartRule['value'] - (float) $orderCartRule['value_tax_excl']
            );
        }

        $shippingData = [];

        if ((float) $order->total_shipping_tax_incl > 0.00 && (float) $order->carrier_tax_rate > 0.00) {
            $shippingData = $this->getShippingData(
                $carrier->name,
                $carrier->name,
                (float) $order->total_shipping_tax_incl,
                (float) $order->carrier_tax_rate
            );
        }

        return array_merge(
            $shippingData,
            $discountData,
            $this->getProductData($order)
        );
    }

    /**
     * Extract product information from Quote entity.
     *
     * @param Order $order
     *
     * @return PaymentItem[]
     *
     * @throws Exception
     */
    protected function getProductData(
        Order $order
    ): array {
        $result = [];

        if ($this->includeProductData($order)) {
            /** @var OrderDetail $product */
            foreach ($order->getOrderDetailList() as $product) {
                $orderDetail = new OrderDetail($product['id_order_detail']);
                $item = new ProductItem($orderDetail);

                $result[] = $item->getItem();
            }
        }

        return $result;
    }

    /**
     * Whether to include product data in payment payload.
     *
     * @param Order $entity
     *
     * @return bool
     */
    public function includeProductData(
        Order $entity
    ): bool {
        $items = $entity->getOrderDetailList();

        return !empty($items);
    }
}
