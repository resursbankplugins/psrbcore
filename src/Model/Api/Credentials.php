<?php
/**
 * Copyright © Resurs Bank AB. All rights reserved.
 * See LICENSE for license details.
 */

declare(strict_types=1);

namespace Resursbank\Core\Model\Api;

use InvalidArgumentException;
use Resursbank\RBEcomPHP\ResursBank;

/**
 * Contains API credentials.
 */
class Credentials
{
    /**
     * @var int
     */
    private $environment = ResursBank::ENVIRONMENT_TEST;

    /**
     * @var string
     */
    private $username = '';

    /**
     * @var string
     */
    private $password = '';

    /**
     * @var string
     */
    private $country = '';

    /**
     * @param int $environment
     * @param string $username
     * @param string $password
     * @param string $country
     */
    public function __construct(
        int $environment,
        string $username,
        string $password,
        string $country
    ) {
        $this->setUsername($username)
            ->setPassword($password)
            ->setEnvironment($environment)
            ->setCountry($country);
    }

    /**
     * @param string $username
     *
     * @return self
     */
    public function setUsername(
        string $username
    ): self {
        if ($username === '') {
            throw new InvalidArgumentException('Username cannot be empty.');
        }

        $this->username = $username;

        return $this;
    }

    /**
     * @return string
     */
    public function getUsername(): string
    {
        return $this->username;
    }

    /**
     * @param string $password
     *
     * @return self
     */
    public function setPassword(
        string $password
    ): self {
        if ($password === '') {
            throw new InvalidArgumentException('Password cannot be empty.');
        }

        $this->password = $password;

        return $this;
    }

    /**
     * @return string
     */
    public function getPassword(): string
    {
        return $this->password;
    }

    /**
     * @param int $environment
     *
     * @return self
     */
    public function setEnvironment(
        int $environment
    ): self {
        if ($environment !== ResursBank::ENVIRONMENT_PRODUCTION &&
            $environment !== ResursBank::ENVIRONMENT_TEST
        ) {
            throw new InvalidArgumentException(
        "Invalid environment value ${environment}. 0 = prod, 1 = test."
            );
        }

        $this->environment = $environment;

        return $this;
    }

    /**
     * @return int
     */
    public function getEnvironment(): int
    {
        return $this->environment;
    }

    /**
     * @param string $country
     *
     * @return self
     */
    public function setCountry(
        string $country
    ): self {
        $this->country = $country;

        return $this;
    }

    /**
     * @return string
     */
    public function getCountry(): string
    {
        return $this->country;
    }
}
