<?php
/**
 * Copyright © Resurs Bank AB. All rights reserved.
 * See LICENSE for license details.
 */

declare(strict_types=1);

namespace Resursbank\Core\Repository;

use Doctrine\ORM\EntityRepository;

/**
 * Repository implementation of the Resurs Bank Order data table.
 */
class ResursbankOrderRepository extends EntityRepository
{

}
