<?php
/**
 * Copyright © Resurs Bank AB. All rights reserved.
 * See LICENSE for license details.
 */

declare(strict_types=1);

namespace Resursbank\Core\Repository;

use Doctrine\Common\Collections\Collection;
use Doctrine\Common\Collections\Criteria;
use Doctrine\Common\Collections\ExpressionBuilder;
use Doctrine\ORM\EntityRepository;
use Resursbank\Core\Entity\ResursbankPaymentMethod;
use Resursbank\Core\Exception\PaymentMethodException;
use Resursbank\Core\Model\Api\Credentials;

/**
 * Repository implementation of the Resurs Bank Payment Method table.
 */
class ResursbankPaymentMethodRepository extends EntityRepository
{
    /**
     * Retrieve an iterable collection of ResursbankPaymentMethod instances
     * matching the following criteria:
     *
     * active = 1
     * code = %[api_username]_[api_environment]
     *
     * @param Credentials $credentials
     * @param bool $all Set to true to ignore supplied credentials
     *
     * @return Collection
     *
     * @throws PaymentMethodException
     */
    public function getList(Credentials $credentials, bool $all = false): Collection
    {
        if ($all) {
            return $this->matching(
                Criteria::create()
                    ->where($this->getExp()->eq('active', 1))
                    ->orderBy(['sortOrder' => Criteria::ASC])
            );
        }

        $code = $credentials->getUsername() .
            '_' .
            $credentials->getEnvironment();

        return $this->matching(
            Criteria::create()
                ->where($this->getExp()->eq('active', 1))
                ->andWhere($this->getExp()->endsWith('code', $code))
                ->orderBy(['sortOrder' => Criteria::ASC])
        );
    }

    /**
     * @return ExpressionBuilder
     *
     * @throws PaymentMethodException
     */
    private function getExp(): ExpressionBuilder
    {
        $exp = Criteria::expr();

        if (!$exp instanceof ExpressionBuilder) {
            throw new PaymentMethodException('Failed to generate criteria.');
        }

        return $exp;
    }
}
