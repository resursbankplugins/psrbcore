<?php
/**
 * Copyright © Resurs Bank AB. All rights reserved.
 * See LICENSE for license details.
 */

declare(strict_types=1);

namespace Resursbank\Core\Controller\Admin;

use Exception;
use PrestaShop\PrestaShop\Core\Domain\Shop\ValueObject\ShopConstraint;
use PrestaShopBundle\Controller\Admin\FrameworkBundleAdminController;
use PrestaShopBundle\Entity\Repository\ShopRepository;
use PrestaShopBundle\Entity\Shop;
use Resursbank\Core\Logger\LoggerInterface;
use Resursbank\Core\Api\Credentials;
use Resursbank\Core\Service\PaymentMethods;
use Symfony\Component\HttpFoundation\RedirectResponse;

/**
 * Sync data from Resurs Bank API to local database.
 */
class Sync extends FrameworkBundleAdminController
{
    /**
     * @var LoggerInterface
     */
    protected $logger;

    /**
     * @var Credentials
     */
    protected $credentials;

    /**
     * @var PaymentMethods
     */
    protected $paymentMethods;

    /**
     * @var ShopRepository
     */
    protected $shopRepository;

    /**
     * @var \psrbcore
     */
    private $module;

    /**
     * @param LoggerInterface $logger
     * @param Credentials $credentials
     * @param PaymentMethods $paymentMethods
     * @param ShopRepository $shopContext
     */
    public function __construct(
        LoggerInterface $logger,
        Credentials $credentials,
        PaymentMethods $paymentMethods,
        ShopRepository $shopContext
    ) {
        parent::__construct();

        $this->logger = $logger;
        $this->credentials = $credentials;
        $this->paymentMethods = $paymentMethods;
        $this->shopRepository = $shopContext;
        $this->module = \Module::getInstanceByName('psrbcore');
    }

    /**
     * Fetch payment methods from API and sync them to database.
     *
     * @return RedirectResponse
     */
    public function execute(): RedirectResponse
    {
        try {
            if ($this->shopRepository->haveMultipleShops()) {
                $shops = $this->shopRepository->findAll();
                /** @var Shop $shop */
                foreach ($shops as $shop) {
                    $credentials = $this->credentials->get(
                        ShopConstraint::shop($shop->getId())
                    );
                    $this->paymentMethods->sync(
                        $credentials
                    );
                }
            } else {
                $this->paymentMethods->sync($this->credentials->get());
            }

            $this->addFlash(
                'success',
                $this->module->l('Successfully synchronized data.')
            );
        } catch (Exception $e) {
            $this->addFlash(
                'error',
                $this->module->l('Failed to synchronize data.')
            );
            $this->logger->exception($e);
        }

        return $this->redirectToRoute('resursbank_admin_config');
    }
}
