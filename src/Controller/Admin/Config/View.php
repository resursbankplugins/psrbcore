<?php
/**
 * Copyright © Resurs Bank AB. All rights reserved.
 * See LICENSE for license details.
 */

declare(strict_types=1);

namespace Resursbank\Core\Controller\Admin\Config;

use Exception;
use PrestaShopBundle\Controller\Admin\FrameworkBundleAdminController;
use Resursbank\Core\Config\Config;
use Resursbank\Core\Config\Form\Builder;
use Resursbank\Core\Logger\LoggerInterface;
use Resursbank\Core\Service\PaymentMethods;
use Resursbank\RBEcomPHP\ResursBank;
use Symfony\Component\HttpFoundation\Response;

/**
 * Sync data from Resurs Bank API to local database.
 */
class View extends FrameworkBundleAdminController
{
    /**
     * @var LoggerInterface
     */
    private $logger;

    /**
     * @var Builder
     */
    private $builder;

    /**
     * @var Config
     */
    private $config;

    /**
     * @var PaymentMethods
     */
    private $paymentMethods;

    /**
     * @var ResursBank
     */
    private $api;

    /**
     * @var \psrbcore
     */
    private $module;

    /**
     * @param LoggerInterface $logger
     * @param Builder $builder
     * @param Config $config
     * @param PaymentMethods $paymentMethods
     */
    public function __construct(
        LoggerInterface $logger,
        Builder $builder,
        Config $config,
        PaymentMethods $paymentMethods
    ) {
        parent::__construct();

        $this->logger = $logger;
        $this->builder = $builder;
        $this->config = $config;
        $this->paymentMethods = $paymentMethods;
        $this->module = \Module::getInstanceByName('psrbcore');
    }

    /**
     * Fetch payment methods from API and sync them to database.
     *
     * @return Response
     *
     * @throws Exception
     *
     * @todo Our view will statically render a table containing our payment methods, we could use HelperList instead, but it may be preferable to render this ourselves so we can centralise it to ECom.
     * @todo It's a little unclear right now what the list of payment methods display. The list won't update as you flip between prod / test. Since both credentials are always displayed this should be addressed.
     */
    public function execute(): Response
    {
        try {
            $validateTestCredentials = $this->validateCredentials(
                ResursBank::ENVIRONMENT_TEST
            );
            $timeoutExceptionTest = $this->api instanceof ResursBank ? $this->api->hasTimeoutException() : false;
            $validateProdCredentials = $this->validateCredentials(
                ResursBank::ENVIRONMENT_PRODUCTION
            );
            $timeoutExceptionProd = $this->api instanceof ResursBank ? $this->api->hasTimeoutException() : false;

            return $this->render('@Modules/psrbcore/views/admin/config.twig', [
                'layoutTitle' => $this->module->l('Resurs Bank'),
                'requireAddonsSearch' => true,
                'enableSidebar' => true,
                'help_link' => '',
                'form' => $this->builder->getForm()->createView(),
                'paymentMethods' => $this->paymentMethods->getList(),
                // @todo We should pass a shopConstraint here for multistore support.
                'validateTestCredentials' => $validateTestCredentials,
                'validateProdCredentials' => $validateProdCredentials,
                'timeoutTestException' => $timeoutExceptionTest,
                'timeoutProdException' => $timeoutExceptionProd,
                'hasTestCredentials' => $this->hasCredentials(
                    ResursBank::ENVIRONMENT_TEST
                ),
                'hasProdCredentials' => $this->hasCredentials(
                    ResursBank::ENVIRONMENT_PRODUCTION
                ),
            ]);
        } catch (Exception $e) {
            $this->logger->exception($e);
            throw $e;
        }
    }

    /**
     * Validate API credentials and add an error message if a set of credentials
     * are inaccurate.
     *
     * @param int $env
     *
     * @return bool
     */
    private function validateCredentials(
        int $env
    ): bool {
        $result = false;

        if ($this->config->hasCredentials($env)) {
            $credentials = $this->config->getCredentials($env);
            $this->api = new ResursBank();

            try {
                $result = $this->api->validateCredentials(
                    $env,
                    $credentials->getUsername(),
                    $credentials->getPassword()
                );
            } catch (Exception $e) {
                $this->logger->exception($e);
            }
        }

        return $result;
    }

    /**
     * @param int $env
     *
     * @return bool
     *
     * @since 1.0.0
     */
    private function hasCredentials(int $env): bool
    {
        return $this->config->hasCredentials($env);
    }
}
