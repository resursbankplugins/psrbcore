<?php
/**
 * Copyright © Resurs Bank AB. All rights reserved.
 * See LICENSE for license details.
 */

declare(strict_types=1);

namespace Resursbank\Core\Service;

use Country;
use Resursbank\Core\Model\Api\Address as ApiAddress;

/**
 * API adapter utilising the EComPHP library.
 *
 * @SuppressWarnings(PHPMD.CouplingBetweenObjects)
 */
class Api
{
    /**
     * Creates address model data from a generic object. Expects the generic
     * object to have the same properties as address data fetched from the API,
     * but it's not required to. Missing properties will be created using
     * default values.
     *
     * @param object $address
     * @param bool|null $isCompany
     * @param string $telephone
     * @return ApiAddress
     * @SuppressWarnings(PHPMD.CyclomaticComplexity)
     */
    public function toAddress(
        object $address,
        bool $isCompany = null,
        string $telephone = ''
    ): ApiAddress {

        return new ApiAddress(
            (
                $isCompany === null &&
                property_exists($address, 'fullName') &&
                (string) $address->fullName !== ''
            ) || $isCompany,
            property_exists($address, 'fullName') ?
                (string) $address->fullName :
                '',
            property_exists($address, 'firstName') ?
                (string) $address->firstName :
                '',
            property_exists($address, 'lastName') ?
                (string) $address->lastName :
                '',
            property_exists($address, 'addressRow1') ?
                (string) $address->addressRow1 :
                '',
            property_exists($address, 'addressRow2') ?
                (string) $address->addressRow2 :
                '',
            property_exists($address, 'postalArea') ?
                (string) $address->postalArea :
                '',
            property_exists($address, 'postalCode') ?
                (string) $address->postalCode :
                '',
            property_exists($address, 'country') ?
                (string) $address->country :
                '',
            $telephone,
            property_exists($address, 'country') ? Country::getByIso($address->country) : 0
        );
    }
}
