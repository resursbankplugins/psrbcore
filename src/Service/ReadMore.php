<?php
/**
 * Copyright © Resurs Bank AB. All rights reserved.
 * See LICENSE for license details.
 */

declare(strict_types=1);

namespace Resursbank\Core\Service;

use Exception;
use PrestaShop\PrestaShop\Adapter\Entity\Context;
use Resursbank\Core\Entity\ResursbankPaymentMethod;
use Resursbank\Core\Logger\Logger;
use Resursbank\RBEcomPHP\ResursBank;

/**
 * Common business logic for Read More functionality.
 *
 * @suppress PhpUnused
 */
class ReadMore
{
    /**
     * @var Logger
     */
    private $log;

    /**
     * @var Context
     */
    protected $context;

    /**
     * @var ResursBank
     */
    private $api;

    /**
     * @param Logger $log
     */
    public function __construct(
        Logger $log
    ) {
        $this->log = $log;
        $this->context = Context::getContext();
        $this->api = new ResursBank();
    }

    /**
     * @param string $linkTitle
     * @param string $modalTitle
     * @param string $id
     * @param ResursbankPaymentMethod|null $method
     * @param string $label
     * @param string $info
     * @param float $price
     * @param bool $useLogo
     *
     * @return array
     * @noinspection PhpUnused
     */
    public function getTemplateData(
        string $linkTitle,
        string $modalTitle,
        string $id,
        ResursbankPaymentMethod $method = null,
        string $label = '',
        string $info = '',
        float $price = 0.0,
        bool $useLogo = true
    ): array {
        $context = Context::getContext();

        return [
            'useReadMore' => $this->isEnabled($method),
            'method' => $method === null ? '' : $method->getCode(),
            'label' => $label,
            'info' => $info,
            'remodalId' => $id,
            'linkTitle' => $linkTitle,
            'modalTitle' => $modalTitle,
            'loaderId' => $id . '-loader',
            'contentId' => $id . '-content',
            'logo' => $useLogo ? $context->link->getMediaLink(
                '/modules/psrbcore/assets/img/logo.png'
            ) : '',
            'price' => $price,
        ];
    }

    /**
     * @param ResursbankPaymentMethod|null $method
     * @return bool
     */
    private function isEnabled(
        ResursbankPaymentMethod $method = null
    ): bool {
        $result = false;

        try {
            $result = (
                !($method === null) &&
                (
                    $this->api->isInternalMethod($method->getType())
                )
            );
        } catch (Exception $e) {
            $this->log->exception($e);
        }

        return $result;
    }
}
