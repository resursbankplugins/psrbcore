<?php
/**
 * Copyright © Resurs Bank AB. All rights reserved.
 * See LICENSE for license details.
 */

declare(strict_types=1);

namespace Resursbank\Core\Service;

use InvalidArgumentException;
use ReflectionClass;
use Resursbank\Core\Exception\MissingPathException;

/**
 * Methods to ease filesystem operations between modules.
 */
class Filesystem
{
    /**
     * Shorthand to resolve path to file within frontend templates dir.
     *
     * @param string $module
     * @param string $file | subdirectory/file.tpl
     * @return string
     * @throws MissingPathException
     */
    public static function getTemplate(
        string $module,
        string $file
    ): string {
        return self::getPath($module, "views/templates/front/${file}");
    }

    /**
     * Shorthand to resolve path to file in sql dir.
     *
     * @param string $module
     * @param string $file | subdirectory/file.sql
     * @return string
     * @throws MissingPathException
     */
    public static function getSql(
        string $module,
        string $file
    ): string {
        return self::getPath($module, "sql/${file}");
    }

    /**
     * Shorthand to resolve path to file in global img dir.
     *
     * @param string $module
     * @param string $file | subdirectory/file.gif
     * @return string
     * @throws MissingPathException
     */
    public static function getImg(
        string $module,
        string $file
    ): string {
        return self::getPath($module, "views/img/${file}");
    }

    /**
     * Resolve path to file within module directory.
     *
     * @param string $module
     * @param string $sub
     * @return string
     * @throws MissingPathException
     */
    public static function getPath(
        string $module,
        string $sub = ''
    ): string {
        if (!class_exists($module)) {
            throw new InvalidArgumentException("Unknown class ${module}");
        }

        $ref = new ReflectionClass($module);

        $path = dirname($ref->getFileName()) . '/' . $sub;

        if (!file_exists($path)) {
            throw new MissingPathException("Missing file ${path}");
        }

        return $path;
    }
}
