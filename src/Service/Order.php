<?php
/**
 * Copyright © Resurs Bank AB. All rights reserved.
 * See LICENSE for license details.
 */
/* @noinspection PhpCastIsUnnecessaryInspection */

declare(strict_types=1);

namespace Resursbank\Core\Service;

use Doctrine\ORM\EntityManager;
use Doctrine\ORM\OptimisticLockException;
use Doctrine\ORM\ORMException;
use Doctrine\Persistence\ObjectRepository;
use InvalidArgumentException;
use Order as CoreOrder;
use PrestaShop\PrestaShop\Core\Domain\Shop\Exception\ShopException;
use PrestaShop\PrestaShop\Core\Domain\Shop\ValueObject\ShopConstraint;
use Resursbank\Core\Config\Config;
use Resursbank\Core\Entity\ResursbankOrder as OrderEntity;
use Resursbank\RBEcomPHP\ResursBank;

/**
 * Implement methods to handle order data.
 */
class Order
{
    /**
     * @var EntityManager
     */
    private $entityManager;

    /**
     * @var Config
     */
    private $config;

    /**
     * @param Config $config
     * @param EntityManager $entityManager
     */
    public function __construct(
        Config $config,
        EntityManager $entityManager
    ) {
        $this->config = $config;
        $this->entityManager = $entityManager;
    }

    /**
     * @throws OptimisticLockException
     * @throws ORMException
     * @throws ShopException
     */
    public function create(
        CoreOrder $order
    ): void {
        $entity = new OrderEntity();

        if ((int) $order->id === 0) {
            throw new InvalidArgumentException('None existing order.');
        }

        $entity->setOrderId((int) $order->id)
            ->setTest(
                $this->config->getEnvironment(
                    ShopConstraint::shop((int) $order->id_shop)
                ) === ResursBank::ENVIRONMENT_TEST
            )
            ->setReadyForCallback(false);

        $this->entityManager->persist($entity);
        $this->entityManager->flush();
    }

    /**
     * @param int $orderId
     *
     * @return OrderEntity
     */
    public function getOrder(
        int $orderId
    ): OrderEntity {
        $order = $this->getRepository()->findOneBy(['orderId' => $orderId]);

        return ($order instanceof OrderEntity) ? $order : new OrderEntity();
    }

    /**
     * @return ObjectRepository
     */
    private function getRepository(): ObjectRepository
    {
        return $this->entityManager->getRepository(OrderEntity::class);
    }
}
