<?php
/**
 * Copyright © Resurs Bank AB. All rights reserved.
 * See LICENSE for license details.
 */

declare(strict_types=1);

namespace Resursbank\Core\Service;

use Exception;
use Language;
use PrestaShopBundle\Entity\Repository\TabRepository;
use PrestaShopDatabaseException;
use PrestaShopException;
use Resursbank\Core\ModuleInterface;
use Symfony\Component\Translation\TranslatorInterface;
use Tab as PrestaTab;

/**
 * Business logic to handle tabs in the admin panel.
 */
class Tab
{
    /**
     * @var TabRepository
     */
    private $repo;

    /**
     * @var TranslatorInterface
     */
    private $translator;

    /**
     * @param TabRepository $repository
     * @param TranslatorInterface $translator
     */
    public function __construct(
        TabRepository $repository,
        TranslatorInterface $translator
    ) {
        $this->repo = $repository;
        $this->translator = $translator;
    }

    /**
     * @param string $moduleName // Not to be confused with module main class
     * @param string $name
     * @param string $parent
     * @param string $title
     * @param string $route
     * @param string $trans
     *
     * @throws PrestaShopDatabaseException
     * @throws PrestaShopException
     */
    public function create(
        string $moduleName,
        string $name,
        string $parent,
        string $title,
        string $route,
        string $trans
    ): void {
        $tab = $this->load($name);

        $tab->active = 1;
        $tab->class_name = $name;
        $tab->route_name = $route;
        $tab->name = [];

        foreach (Language::getLanguages() as $lang) {
            $tab->name[$lang['id_lang']] = $this->translator->trans(
                $title,
                [],
                $trans,
                $lang['locale']
            );
        }

        $tab->id_parent = (int) $this->repo->findOneIdByClassName($parent);
        $tab->module = $moduleName;
        $tab->save();
    }

    /**
     * @throws PrestaShopException
     * @throws PrestaShopDatabaseException
     * @throws Exception
     */
    public function load(string $name): PrestaTab
    {
        $tab = false;
        $id = (int) $this->repo->findOneIdByClassName($name);

        if ($id !== 0) {
            $tab = new PrestaTab($id);
        }

        return $tab ?: new PrestaTab();
    }

    /**
     * @param string $name
     *
     * @return void
     *
     * @throws PrestaShopDatabaseException
     * @throws PrestaShopException
     */
    public function delete(
        string $name
    ): void {
        $tab = $this->load($name);

        if ((int) $tab->id !== 0) {
            $tab->delete();
        }
    }

    /**
     * @param string $name
     *
     * @return void
     *
     * @throws PrestaShopDatabaseException
     * @throws PrestaShopException
     */
    public function deactivate(
        string $name
    ): void {
        $tab = $this->load($name);

        /*
         * When a modules install method executes the parent method PrestaShop
         * will call the enable method, which in turns calls this method to
         * enable the tabs before they exist. For this reason we cannot throw an
         * error at this junction in case the tab does not exist.
         */
        if ((int) $tab->id !== 0) {
            $tab->active = false;
            $tab->save();
        }
    }

    /**
     * @param string $name
     *
     * @return void
     *
     * @throws PrestaShopDatabaseException
     * @throws PrestaShopException
     * @throws Exception
     */
    public function activate(
        string $name
    ): void {
        $tab = $this->load($name);

        /*
         * When a modules install method executes the parent method PrestaShop
         * will call the enable method, which in turns calls this method to
         * enable the tabs before they exist. For this reason we cannot throw an
         * error at this junction in case the tab does not exist.
         */
        if ((int) $tab->id !== 0) {
            $tab->active = true;
            $tab->save();
        }
    }

    /**
     * The methods to create / activate / delete / deactivate will be called in
     * context of the installation procedure for our modules. The config files
     * of this module won't load until the subsequent request post-installation.
     * Because of that we cannot utilize the container within the installation
     * process itself to access this service class. For that reason.
     *
     * In short, this method allows us to access instantiate this class within
     * the context of the module installation / uninstall processes (remember
     * that the install() method may call the uninstall() method within the
     * same request cycle for rollbacks).
     *
     * @param ModuleInterface $module
     *
     * @return static
     *
     * @throws Exception
     */
    public static function getInstance(
        ModuleInterface $module
    ): self {
        $repository = $module->get('prestashop.core.admin.tab.repository');
        $translator = $module->get('translator');

        if (!$repository instanceof TabRepository) {
            throw new Exception('Failed to resolve tab repo from container.');
        }

        if (!$translator instanceof TranslatorInterface) {
            throw new Exception('Failed to resolve translator from container.');
        }

        return new self($repository, $translator);
    }
}
