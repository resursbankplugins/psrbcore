<?php
/**
 * Copyright © Resurs Bank AB. All rights reserved.
 * See LICENSE for license details.
 */

declare(strict_types=1);

namespace Resursbank\Core\Service;

use function count;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\EntityManager;
use Doctrine\ORM\OptimisticLockException;
use Doctrine\ORM\ORMException;
use Doctrine\Persistence\ObjectRepository;
use Exception;
use function is_array;
use function is_numeric;
use function is_string;
use function json_encode;
use JsonException;
use PrestaShop\PrestaShop\Adapter\Entity\Context;
use PrestaShop\PrestaShop\Adapter\Entity\Hook;
use Resursbank\Core\Api\Connection;
use Resursbank\Core\Config\Config;
use Resursbank\Core\Entity\ResursbankPaymentMethod as Method;
use Resursbank\Core\Exception\PaymentMethodException;
use Resursbank\Core\Model\Api\Credentials;
use Resursbank\Core\Repository\ResursbankPaymentMethodRepository;
use stdClass;

/**
 * Implement methods to handle payment methods.
 */
class PaymentMethods
{
    /**
     * @var Connection
     */
    private $api;

    /**
     * @var EntityManager
     */
    private $entityManager;

    /**
     * @var Config
     */
    private $config;

    /**
     * @var ResursbankPaymentMethodRepository
     */
    private $repository;

    /**
     * @param Config $config
     * @param Connection $api
     * @param EntityManager $entityManager
     * @param ResursbankPaymentMethodRepository $repository
     */
    public function __construct(
        Config $config,
        Connection $api,
        EntityManager $entityManager,
        ResursbankPaymentMethodRepository $repository
    ) {
        $this->config = $config;
        $this->api = $api;
        $this->entityManager = $entityManager;
        $this->repository = $repository;
    }

    /**
     * Deactivate all payment methods
     *
     * @return void
     *
     * @throws ORMException
     * @throws PaymentMethodException
     */
    private function deactivateMethods(): void
    {
        $env = $this->config->getEnvironment();
        $credentials = $this->config->getCredentials($env);
        $methods = $this->repository->getList($credentials, true);

        foreach ($methods as $method) {
            $method->setActive(false);
            $this->entityManager->persist($method);
        }

        $this->entityManager->flush();
    }

    /**
     * @param Credentials $credentials
     *
     * @throws JsonException
     * @throws PaymentMethodException
     * @throws ORMException
     * @throws OptimisticLockException
     * @throws Exception
     */
    public function sync(
        Credentials $credentials
    ): void {
        $this->deactivateMethods();

        // Retrieve list of methods from API.
        $methods = $this->api
            ->getConnection($credentials)
            ->getPaymentMethods();

        // Convert to Entity objects.
        $converted = $this->convertCollection($methods, $credentials);

        // Sync each Entity to the database.
        foreach ($converted as $method) {
            $this->entityManager->persist($method);
        }

        // Commit changes to database.
        $this->entityManager->flush();

        Hook::exec('resursBankAfterPaymentMethodSync', [
            'methods' => $converted,
        ]);
    }

    /**
     * @param array $methods
     * @param Credentials $credentials
     *
     * @return array<Method>
     *
     * @throws JsonException
     * @throws PaymentMethodException
     */
    public function convertCollection(
        array $methods,
        Credentials $credentials
    ): array {
        $result = [];

        $order = 0;
        foreach ($methods as $method) {
            $result[] = $this
                ->convert($method, $credentials)
                ->setSortOrder(++$order);
        }

        return $result;
    }

    /**
     * @param stdClass $data
     * @param Credentials $credentials
     *
     * @return Method
     *
     * @throws PaymentMethodException
     * @throws JsonException
     *
     * @todo Remember to test that updates applied for a payment method at Resurs Bank are written to the corresponding row in our payment method table, I cannot test this right now.
     */
    public function convert(
        stdClass $data,
        Credentials $credentials
    ): Method {
        // Set method identifier (raw ID from API).
        if (!property_exists($data, 'id') || !is_string($data->id)) {
            throw new PaymentMethodException(
                'Missing or invalid id property.'
            );
        }

        // Generate unique method code (identifier made unique by API settings).
        $code = implode('_', [
            'resursbank',
            strtolower($data->id),
            $credentials->getUsername(),
            $credentials->getEnvironment(),
        ]);

        // Get new or existing entity.
        try {
            $method = $this->getMethod($code);
        } catch (Exception $e) {
            $method = new Method();
        }

        // Assign raw identifier from Resurs Bank API response.
        $method->setIdentifier($data->id);

        // Set method title (description).
        if (
            !property_exists($data, 'description')
            || !is_string($data->description)
        ) {
            throw new PaymentMethodException(
                'Missing or invalid description property.'
            );
        }

        $method->setTitle($data->description);

        // Set method min order total (minLimit).
        if (
            !property_exists($data, 'minLimit') ||
            !is_numeric($data->minLimit)
        ) {
            throw new PaymentMethodException(
                'Missing or invalid minLimit property.'
            );
        }

        $method->setMinOrderTotal((float) $data->minLimit);

        // Set method max order total (maxLimit).
        if (
            !property_exists($data, 'maxLimit') ||
            !is_numeric($data->maxLimit)
        ) {
            throw new PaymentMethodException(
                'Missing or invalid maxLimit property.'
            );
        }

        $method->setMaxOrderTotal((float) $data->maxLimit);

        // Generate unique payment method code.
        $method->setCode($code);

        // Set complete raw data (i.e. complete payment method conf from API).
        $method->setRaw(json_encode($data, JSON_THROW_ON_ERROR));

        // Set default values.
        $method->setActive(true);
        $method->setOrderStatus(Method::DEFAULT_ORDER_STATUS);
        $method->setSpecificCountry($credentials->getCountry());

        return $method;
    }

    /**
     * @return ObjectRepository
     */
    private function getRepository(): ObjectRepository
    {
        return $this->entityManager->getRepository(Method::class);
    }

    /**
     * @param string $code
     *
     * @return Method
     *
     * @throws Exception
     */
    public function getMethod(
        string $code
    ): Method {
        /** @var Method[] $method */
        $method = $this->getRepository()->findBy(['code' => $code]);

        if (is_array($method) &&
            (count($method) !== 1 || $method[0]->getId() === 0)
        ) {
            throw new Exception(
                'Unable to find payment method with code ' . $code
            );
        }

        return $method[0];
    }

    /**
     * @return Collection|null
     *
     * @throws PaymentMethodException
     */
    public function getList(): ?Collection
    {
        $result = null;

        // @todo If we want to support multistore we should pass a shopConstraint here.
        $env = $this->config->getEnvironment();

        if ($this->config->hasCredentials($env)) {
            // @todo If we want to support multistore we should pass a shopConstraint here.
            $credentials = $this->config->getCredentials($env);

            /* @phpstan-ignore-next-line */
            $credentials->setCountry(Context::getContext()->country->iso_code);

            $result = $this->repository->getList($credentials);
        }

        return $result;
    }
}
