<?php
/**
 * Copyright © Resurs Bank AB. All rights reserved.
 * See LICENSE for license details.
 */

declare(strict_types=1);

namespace Resursbank\Core\Service;

/**
 * This class lets autoloading work for our modules and associated libraries
 * even when composer has not been utilised to generate a classmap. This is
 * required for PrestaShop modules since Composer is not required.
 */
final class Autoloader
{
    /**
     * @var string[]
     */
    private static $index = [
        'TorneLIB\MODULE_CRYPTO' => 'tornevall/tornelib-php-crypto/src/MODULE_CRYPTO.php',
        'TorneLIB\Data\Aes' => 'tornevall/tornelib-php-crypto/src/Data/Aes.php',
        'TorneLIB\Data\Compress' => 'tornevall/tornelib-php-crypto/src/Data/Compress.php',
        'TorneLIB\Data\Crypto' => 'tornevall/tornelib-php-crypto/src/Data/Crypto.php',
        'TorneLIB\Data\Password' => 'tornevall/tornelib-php-crypto/src/Data/Password.php',
        'TorneLIB\Exception\Constants' => 'tornevall/tornelib-php-errorhandler/src/Exception/Constants.php',
        'TorneLIB\Exception\ExceptionHandler' => 'tornevall/tornelib-php-errorhandler/src/Exception/ExceptionHandler.php',
        'TorneLIB\Flags' => 'tornevall/tornelib-php-flags/src/Flags.php',
        'TorneLIB\Config\Flag' => 'tornevall/tornelib-php-flags/src/Config/Flag.php',
        'TorneLIB\IO\Data\Arrays' => 'tornevall/tornelib-php-io/src/IO/Data/Arrays.php',
        'TorneLIB\IO\Data\Content' => 'tornevall/tornelib-php-io/src/IO/Data/Content.php',
        'TorneLIB\IO\Data\Strings' => 'tornevall/tornelib-php-io/src/IO/Data/Strings.php',
        'TorneLIB\IO\Bridge' => 'tornevall/tornelib-php-io/src/IO/Bridge.php',
        'TorneLIB\MODULE_IO' => 'tornevall/tornelib-php-io/src/MODULE_IO.php',
        'TorneLIB\Helpers\Browsers' => 'tornevall/tornelib-php-netcurl/src/Helpers/Browsers.php',
        'TorneLIB\Helpers\GenericParser' => 'tornevall/tornelib-php-netcurl/src/Helpers/GenericParser.php',
        'TorneLIB\Helpers\NetUtils' => 'tornevall/tornelib-php-netcurl/src/Helpers/NetUtils.php',
        'TorneLIB\Helpers\SimpleDomParser' => 'tornevall/tornelib-php-netcurl/src/Helpers/SimpleDomParser.php',
        'TorneLIB\Model\Interfaces\WrapperInterface' => 'tornevall/tornelib-php-netcurl/src/Model/Interfaces/WrapperInterface.php',
        'TorneLIB\Model\Type\AuthSource' => 'tornevall/tornelib-php-netcurl/src/Model/Type/AuthSource.php',
        'TorneLIB\Model\Type\AuthType' => 'tornevall/tornelib-php-netcurl/src/Model/Type/AuthType.php',
        'TorneLIB\Model\Type\DataType' => 'tornevall/tornelib-php-netcurl/src/Model/Type/DataType.php',
        'TorneLIB\Model\Type\RequestMethod' => 'tornevall/tornelib-php-netcurl/src/Model/Type/RequestMethod.php',
        'TorneLIB\Module\Config\WrapperConfig' => 'tornevall/tornelib-php-netcurl/src/Module/Config/WrapperConfig.php',
        'TorneLIB\Module\Config\WrapperConstants' => 'tornevall/tornelib-php-netcurl/src/Module/Config/WrapperConstants.php',
        'TorneLIB\Module\Config\WrapperCurlOpt' => 'tornevall/tornelib-php-netcurl/src/Module/Config/WrapperCurlOpt.php',
        'TorneLIB\Module\Config\WrapperDriver' => 'tornevall/tornelib-php-netcurl/src/Module/Config/WrapperDriver.php',
        'TorneLIB\Module\Config\WrapperSSL' => 'tornevall/tornelib-php-netcurl/src/Module/Config/WrapperSSL.php',
        'TorneLIB\Module\Network\Wrappers\CurlWrapper' => 'tornevall/tornelib-php-netcurl/src/Module/Network/Wrappers/CurlWrapper.php',
        'TorneLIB\Module\Network\Wrappers\RssWrapper' => 'tornevall/tornelib-php-netcurl/src/Module/Network/Wrappers/RssWrapper.php',
        'TorneLIB\Module\Network\Wrappers\SimpleStreamWrapper' => 'tornevall/tornelib-php-netcurl/src/Module/Network/Wrappers/SimpleStreamWrapper.php',
        'TorneLIB\Module\Network\Wrappers\SoapClientWrapper' => 'tornevall/tornelib-php-netcurl/src/Module/Network/Wrappers/SoapClientWrapper.php',
        'TorneLIB\Module\Network\NetWrapper' => 'tornevall/tornelib-php-netcurl/src/Module/Network/NetWrapper.php',
        'TorneLIB\MODULE_CURL' => 'tornevall/tornelib-php-netcurl/src/MODULE_CURL.php',
        'TorneLIB\Module\Network\Address' => 'tornevall/tornelib-php-network/src/Module/Network/Address.php',
        'TorneLIB\Module\Network\Cookie' => 'tornevall/tornelib-php-network/src/Module/Network/Cookie.php',
        'TorneLIB\Module\Network\Domain' => 'tornevall/tornelib-php-network/src/Module/Network/Domain.php',
        'TorneLIB\Module\Network\Proxy' => 'tornevall/tornelib-php-network/src/Module/Network/Proxy.php',
        'TorneLIB\Module\Network\Statics' => 'tornevall/tornelib-php-network/src/Module/Network/Statics.php',
        'TorneLIB\Module\DeprecateNet' => 'tornevall/tornelib-php-network/src/Module/DeprecateNet.php',
        'TorneLIB\Module\Ip' => 'tornevall/tornelib-php-network/src/Module/Ip.php',
        'TorneLIB\Module\Network' => 'tornevall/tornelib-php-network/src/Module/Network.php',
        'TorneLIB\MODULE_NETWORK' => 'tornevall/tornelib-php-network/src/Network.php',
        'TorneLIB\Utils\Generic' => 'tornevall/tornelib-php-utils/src/Utils/Generic.php',
        'TorneLIB\Utils\ImageHandler' => 'tornevall/tornelib-php-utils/src/Utils/ImageHandler.php',
        'TorneLIB\Utils\Ini' => 'tornevall/tornelib-php-utils/src/Utils/Ini.php',
        'TorneLIB\Utils\Memory' => 'tornevall/tornelib-php-utils/src/Utils/Memory.php',
        'TorneLIB\Utils\Security' => 'tornevall/tornelib-php-utils/src/Utils/Security.php',
        'TorneLIB\Utils\WordPress' => 'tornevall/tornelib-php-utils/src/Utils/WordPress.php',
        'TorneLIB\Helpers\Version' => 'tornevall/tornelib-php-version/src/Helpers/Version.php',
        'Resursbank\RBEcomPHP\ResursBank' => 'resursbank/ecomphp/source/classes/rbapiloader.php',
        'RESURS_EXCEPTIONS' => 'resursbank/ecomphp-deprecated/src/RESURS/EXCEPTIONS.php',
        'Resursbank\RBEcomPHP\RESURS_AFTERSHOP_RENDER_TYPES' => 'resursbank/ecomphp-deprecated/src/RESURS_AFTERSHOP_RENDER_TYPES.php',
        'Resursbank\RBEcomPHP\RESURS_CALLBACK_REACHABILITY' => 'resursbank/ecomphp-deprecated/src/RESURS_CALLBACK_REACHABILITY.php',
        'Resursbank\RBEcomPHP\RESURS_CALLBACK_TYPES' => 'resursbank/ecomphp-deprecated/src/RESURS_CALLBACK_TYPES.php',
        'Resursbank\RBEcomPHP\RESURS_COUNTRY' => 'resursbank/ecomphp-deprecated/src/RESURS_COUNTRY.php',
        'Resursbank\RBEcomPHP\RESURS_CURL_METHODS' => 'resursbank/ecomphp-deprecated/src/RESURS_CURL_METHODS.php',
        'Resursbank\RBEcomPHP\RESURS_DEPRECATED_FLOW' => 'resursbank/ecomphp-deprecated/src/RESURS_DEPRECATED_FLOW.php',
        'Resursbank\RBEcomPHP\RESURS_ENVIRONMENTS' => 'resursbank/ecomphp-deprecated/src/RESURS_ENVIRONMENTS.php',
        'Resursbank\RBEcomPHP\RESURS_FLOW_TYPES' => 'resursbank/ecomphp-deprecated/src/RESURS_FLOW_TYPES.php',
        'Resursbank\RBEcomPHP\RESURS_GETPAYMENT_REQUESTTYPE' => 'resursbank/ecomphp-deprecated/src/RESURS_GETPAYMENT_REQUESTTYPE.php',
        'Resursbank\RBEcomPHP\RESURS_PAYMENT_STATUS_RETURNCODES' => 'resursbank/ecomphp-deprecated/src/RESURS_PAYMENT_STATUS_RETURNCODES.php',
        'Resursbank\RBEcomPHP\RESURS_STATUS_RETURN_CODES' => 'resursbank/ecomphp-deprecated/src/RESURS_STATUS_RETURN_CODES.php',
        'ResursException' => 'resursbank/ecomphp-deprecated/src/ResursException.php'
    ];

    /**
     * Load a file from one of our modules or associated libraries.
     *
     * @param string $name
     * @return void
     */
    public static function load(string $name): void
    {
        $data = explode('\\', $name);

        if (count($data) < 2) {
            return;
        }

        $vendor = strtolower($data[0]);
        $package = strtolower($data[1]);
        unset($data[0], $data[1]);
        $path = '';
        $lib = _PS_MODULE_DIR_ . 'psrbcore/vendor';

        if ($vendor === 'resursbank') {
            if (in_array($package, ['core', 'simplified', 'partpayment', 'ordermanagement'])) {
                $path = _PS_MODULE_DIR_ . "psrb${package}/src/" . implode('/', $data) . '.php';
            } else if ($package === 'ecommerce') {
                $path = "${lib}/${vendor}/ecomphp/src/" . implode('/', $data) . '.php';
            }
        }

        if ($path === '' && isset(self::$index[$name])) {
            $path = $lib . '/' . self::$index[$name];
        }

        if ($path !== '' && file_exists($path)) {
            require_once($path);
        }
    }
}

spl_autoload_register('Resursbank\Core\Service\Autoloader::load');
