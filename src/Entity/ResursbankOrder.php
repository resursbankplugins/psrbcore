<?php
/**
 * Copyright © Resurs Bank AB. All rights reserved.
 * See LICENSE for license details.
 */

declare(strict_types=1);

namespace Resursbank\Core\Entity;

use Doctrine\ORM\Mapping as ORM;
use InvalidArgumentException;

/**
 * NOTE: This class is named in a weird way to make sure it works with
 * Prestashop's internal Entity -> Table mapping
 * (Entity model -> _DB_PREFIX_ . [entity model class in snake casing]).
 *
 * @ORM\Table()
 * @ORM\Entity(repositoryClass="Resursbank\Core\Repository\ResursbankOrderRepository")
 * @ORM\HasLifecycleCallbacks()
 */
class ResursbankOrder
{
    /**
     * @var int
     *
     * @ORM\Id
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer", name="id", nullable=false, scale=10, options={"unsigned":true})
     */
    private $id;

    /**
     * @var int
     *
     * @ORM\Column(type="integer", name="order_id", nullable=false, scale=10, options={"unsigned":true})
     */
    private $orderId;

    /**
     * @var bool
     *
     * @ORM\Column(type="boolean", nullable=false, length=1)
     */
    private $test;

    /**
     * @var bool
     *
     * @ORM\Column(type="boolean", nullable=false, length=1, options={"default": 0})
     */
    private $readyForCallback;

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @return int
     */
    public function getOrderId(): int
    {
        return $this->orderId;
    }

    /**
     * @return bool
     */
    public function getReadyForCallback(): bool
    {
        return $this->readyForCallback;
    }

    /**
     * @param bool $ready
     * @return $this
     */
    public function setReadyForCallback(bool $ready): ResursbankOrder
    {
        $this->readyForCallback = $ready;

        return $this;
    }

    /**
     * @param int $orderId
     *
     * @return ResursbankOrder
     */
    public function setOrderId(int $orderId): ResursbankOrder
    {
        if ($orderId === 0) {
            throw new InvalidArgumentException('Order ID not supplied.');
        }

        $this->orderId = $orderId;

        return $this;
    }

    /**
     * @return bool
     */
    public function getTest(): bool
    {
        return $this->test ?? false;
    }

    /**
     * @param bool $test
     *
     * @return $this
     */
    public function setTest(
        bool $test
    ): ResursbankOrder {
        $this->test = $test;

        return $this;
    }
}
