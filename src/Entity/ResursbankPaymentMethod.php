<?php
/**
 * Copyright © Resurs Bank AB. All rights reserved.
 * See LICENSE for license details.
 */

declare(strict_types=1);

namespace Resursbank\Core\Entity;

use DateTime;
use Doctrine\ORM\Mapping as ORM;
use InvalidArgumentException;
use JsonException;

/**
 * NOTE: This class is named in a weird way to make sure it works with
 * Prestashop's internal Entity -> Table mapping
 * (Entity model -> _DB_PREFIX_ . [entity model class in snake casing]).
 *
 * @ORM\Table()
 * @ORM\Entity(repositoryClass="Resursbank\Core\Repository\ResursbankPaymentMethodRepository")
 * @ORM\HasLifecycleCallbacks()
 */
class ResursbankPaymentMethod
{
    /**
     * @var string
     */
    public const DEFAULT_ORDER_STATUS = 'pending_payment';

    /**
     * @var int
     *
     * @ORM\Id
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer", name="method_id", nullable=false, scale=5, options={"unsigned":true})
     */
    private $methodId;

    /**
     * @var string
     *
     * @ORM\Column(type="string", nullable=false, length=255)
     */
    private $identifier;

    /**
     * @var string
     *
     * @ORM\Column(type="string", unique=true, nullable=false, length=255)
     */
    private $code;

    /**
     * @var bool
     *
     * @ORM\Column(type="boolean", nullable=false, length=1)
     */
    private $active;

    /**
     * @var string
     *
     * @ORM\Column(type="string", nullable=false, length=255)
     */
    private $title;

    /**
     * @var int
     *
     * @ORM\Column(type="integer", name="sort_order", nullable=false, scale=5, options={"unsigned":true, "default":0})
     */
    private $sortOrder;

    /**
     * @var float
     *
     * @ORM\Column(type="decimal", name="min_order_total", precision=15, scale=5, nullable=false, options={"unsigned":true, "default":0.0})
     */
    private $minOrderTotal;

    /**
     * @var float
     *
     * @ORM\Column(type="decimal", name="max_order_total", precision=15, scale=5, nullable=false, options={"unsigned":true, "default":0.0})
     */
    private $maxOrderTotal;

    /**
     * @var string
     *
     * @ORM\Column(type="string", name="order_status", nullable=false, length=255)
     */
    private $orderStatus;

    /**
     * @var string
     *
     * @ORM\Column(type="string")
     */
    private $raw;

    /**
     * @var string
     */
    private $rawJson;

    /**
     * @var DateTime
     *
     * @ORM\Column(type="datetime", name="created_at", nullable=false)
     */
    private $createdAt;

    /**
     * @var DateTime
     *
     * @ORM\Column(type="datetime", name="updated_at", nullable=false)
     */
    private $updatedAt;

    /**
     * @ORM\PrePersist
     * @ORM\PreUpdate
     */
    public function updatedTimestamps(): void
    {
        $timestamp = new DateTime();

        $this->setUpdatedAt($timestamp);

        if ($this->getCreatedAt() === null) {
            $this->setCreatedAt($timestamp);
        }
    }

    /**
     * @var string
     *
     * @ORM\Column(type="string", name="specificcountry", nullable=false, length=2)
     */
    private $specificCountry;

    /**
     * @return int
     */
    public function getMethodId(): int
    {
        return $this->methodId;
    }

    /**
     * @return string
     */
    public function getIdentifier(): string
    {
        return $this->identifier;
    }

    /**
     * @param string $identifier
     *
     * @return $this
     *
     * @throws InvalidArgumentException
     */
    public function setIdentifier(
        string $identifier
    ): ResursbankPaymentMethod {
        if ($identifier === '') {
            throw new InvalidArgumentException('Identifier may not be empty.');
        }

        $this->identifier = $identifier;

        return $this;
    }

    /**
     * @return string
     */
    public function getCode(): string
    {
        return $this->code;
    }

    /**
     * @param string $code
     *
     * @return $this
     *
     * @throws InvalidArgumentException
     */
    public function setCode(
        string $code
    ): ResursbankPaymentMethod {
        if ($code === '') {
            throw new InvalidArgumentException('Code may not be empty.');
        }

        $this->code = $code;

        return $this;
    }

    /**
     * @return bool
     */
    public function getActive(): bool
    {
        return $this->active;
    }

    /**
     * @param bool $active
     *
     * @return $this
     */
    public function setActive(
        bool $active
    ): ResursbankPaymentMethod {
        $this->active = $active;

        return $this;
    }

    /**
     * @return string
     */
    public function getTitle(): string
    {
        return $this->title;
    }

    /**
     * @param string $title
     *
     * @return $this
     *
     * @throws InvalidArgumentException
     */
    public function setTitle(
        string $title
    ): ResursbankPaymentMethod {
        if ($title === '') {
            throw new InvalidArgumentException('Title may not be empty.');
        }

        $this->title = $title;

        return $this;
    }

    /**
     * @return int
     */
    public function getSortOrder(): int
    {
        return $this->sortOrder;
    }

    /**
     * @param int $sortOrder
     *
     * @return $this
     *
     * @throws InvalidArgumentException
     */
    public function setSortOrder(
        int $sortOrder
    ): ResursbankPaymentMethod {
        if ($sortOrder < 0) {
            throw new InvalidArgumentException(
                'Sort order cannot be lower than 0.'
            );
        }

        $this->sortOrder = $sortOrder;

        return $this;
    }

    /**
     * @return float
     */
    public function getMinOrderTotal(): float
    {
        return (float)$this->minOrderTotal;
    }

    /**
     * @param float $minOrderTotal
     *
     * @return $this
     *
     * @throws InvalidArgumentException
     */
    public function setMinOrderTotal(
        float $minOrderTotal
    ): ResursbankPaymentMethod {
        if ($minOrderTotal < 0) {
            throw new InvalidArgumentException(
                'Minimum order total cannot be lower than 0.'
            );
        }

        $this->minOrderTotal = $minOrderTotal;

        return $this;
    }

    /**
     * @return float
     */
    public function getMaxOrderTotal(): float
    {
        return (float)$this->maxOrderTotal;
    }

    /**
     * @param float $maxOrderTotal
     *
     * @return $this
     *
     * @throws InvalidArgumentException
     */
    public function setMaxOrderTotal(
        float $maxOrderTotal
    ): ResursbankPaymentMethod {
        if ($maxOrderTotal < 0) {
            throw new InvalidArgumentException(
                'Maximum order total cannot be lower than 0.'
            );
        }

        $this->maxOrderTotal = $maxOrderTotal;

        return $this;
    }

    /**
     * @return string
     */
    public function getOrderStatus(): string
    {
        return $this->orderStatus;
    }

    /**
     * @param string $orderStatus
     *
     * @return $this
     *
     * @throws InvalidArgumentException
     */
    public function setOrderStatus(
        string $orderStatus
    ): ResursbankPaymentMethod {
        if ($orderStatus === '') {
            throw new InvalidArgumentException(
                'Order status may not be empty.'
            );
        }

        $this->orderStatus = $orderStatus;

        return $this;
    }

    /**
     * @return string
     */
    public function getRaw(): string
    {
        return $this->raw;
    }

    /**
     * @param $key
     * @return mixed
     * @throws JsonException
     */
    private function getRawJson($key)
    {
        if (empty($this->rawJson)) {
            $this->rawJson = json_decode($this->getRaw(), true, 512, JSON_THROW_ON_ERROR);
        }

        return isset($this->rawJson[$key]) ? $this->rawJson[$key] : '';
    }

    /**
     * @return string
     * @throws JsonException
     */
    public function getDescription(): string
    {
        return $this->getRawJson('description');
    }

    /**
     * @return mixed|string
     * @throws JsonException
     */
    public function getSpecificType(): string
    {
        return $this->getRawJson('specificType');
    }

    /**
     * CustomerType, used to be returned as a string or an array from Resurs depending on the number of types
     * supported. We always want this as an array.
     *
     * @return array
     * @throws JsonException
     */
    public function getCustomerType(): array
    {
        return (array)$this->getRawJson('customerType');
    }

    /**
     * @return mixed|string
     * @throws JsonException
     */
    public function getType(): string
    {
        return $this->getRawJson('type');
    }

    /**
     * @return mixed|string
     * @throws JsonException
     */
    public function getId(): string
    {
        return $this->getRawJson('id');
    }

    /**
     * @param string $raw
     *
     * @return $this
     */
    public function setRaw(
        string $raw
    ): ResursbankPaymentMethod {
        $this->raw = $raw;

        return $this;
    }

    /**
     * @return DateTime
     */
    public function getCreatedAt(): ?DateTime
    {
        return $this->createdAt;
    }

    /**
     * @param DateTime $createdAt
     *
     * @return $this
     */
    public function setCreatedAt(
        DateTime $createdAt
    ): ResursbankPaymentMethod {
        $this->createdAt = $createdAt;

        return $this;
    }

    /**
     * @return DateTime
     */
    public function getUpdatedAt(): ?DateTime
    {
        return $this->updatedAt;
    }

    /**
     * @param DateTime $updatedAt
     *
     * @return $this
     */
    public function setUpdatedAt(
        DateTime $updatedAt
    ): ResursbankPaymentMethod {
        $this->updatedAt = $updatedAt;

        return $this;
    }

    /**
     * @return string
     */
    public function getSpecificCountry(): string
    {
        return $this->specificCountry;
    }

    /**
     * @param string $specificCountry
     *
     * @return $this
     *
     * @throws InvalidArgumentException
     */
    public function setSpecificCountry(
        string $specificCountry
    ): ResursbankPaymentMethod {
        if (!preg_match('/\A[a-z]{2}\z/i', $specificCountry)) {
            throw new InvalidArgumentException(
                'Country ISO must be 2 characters long in the following ' .
                'format: [a-zA-Z]. Lowercase chars will be cast to uppercase.'
            );
        }

        $this->specificCountry = $specificCountry;

        return $this;
    }
}
