<?php
/**
 * Copyright © Resurs Bank AB. All rights reserved.
 * See LICENSE for license details.
 */

declare(strict_types=1);

namespace Resursbank\Core\Api;

use PrestaShop\PrestaShop\Core\Domain\Shop\ValueObject\ShopConstraint;
use Resursbank\Core\Config\Config;
use Resursbank\Core\Model\Api\Credentials as CredentialsModel;

/**
 * Implement methods to API credentials.
 */
class Credentials
{
    /**
     * @var Config
     */
    private $config;

    /**
     * @param Config $config
     */
    public function __construct(
        Config $config
    ) {
        $this->config = $config;
    }

    /**
     * @param ShopConstraint|null $shopConstraint
     * @param int|null $env
     * @return CredentialsModel
     */
    public function get(
        ?ShopConstraint $shopConstraint = null,
        ?int $env = null
    ): CredentialsModel {
        return $this->config->getCredentials(
            $env ?? $this->config->getEnvironment($shopConstraint),
            $shopConstraint
        );
    }
}
