<?php
/**
 * Copyright © Resurs Bank AB. All rights reserved.
 * See LICENSE for license details.
 */

declare(strict_types=1);

namespace Resursbank\Core\Config\Form;

use Exception;
use PrestaShop\PrestaShop\Core\Form\FormDataProviderInterface;
use Resursbank\Core\Config\Config;
use Resursbank\Core\Logger\Logger;
use Resursbank\RBEcomPHP\ResursBank;
use Symfony\Component\Translation\TranslatorInterface;

/**
 * Configuration page data provider.
 */
class DataProvider implements FormDataProviderInterface
{
    /**
     * @var Config
     */
    private $config;

    /**
     * @var Logger
     */
    private $logger;

    /**
     * @var TranslatorInterface
     */
    private $translator;

    private $module;

    /**
     * @param Config $config
     * @param Logger $logger
     * @param TranslatorInterface $translator
     */
    public function __construct(
        Config $config,
        Logger $logger,
        TranslatorInterface $translator
    ) {
        $this->config = $config;
        $this->logger = $logger;
        $this->translator = $translator;
        $this->module = \Module::getInstanceByName('psrbcore');
    }

    /**
     * @inheridoc
     * @return array
     */
    public function getData(): array
    {
        $test = ResursBank::ENVIRONMENT_TEST;
        $prod = ResursBank::ENVIRONMENT_PRODUCTION;

        return [
            'api' => [
                'flow' => $this->config->getFlow(),
                'environment' => $this->config->getEnvironment(),
                "username_{$test}" => $this->config->getUsername($test),
                "username_{$prod}" => $this->config->getUsername($prod),
            ],
            'advanced' => [
                'debug' => $this->config->isDebugEnabled(),
            ],
        ];
    }

    /**
     * {@inheritdoc}
     * @todo The parent throws a specific type of Exception which we may wish to utilize, not sure if they are rendered in a specific way.
     */
    public function setData(
        array $data
    ): array {
        return array_filter([
            $this->setFlow($data),
            $this->setEnvironment($data),
            $this->setUsername($data, ResursBank::ENVIRONMENT_TEST),
            $this->setUsername($data, ResursBank::ENVIRONMENT_PRODUCTION),
            $this->setPassword($data, ResursBank::ENVIRONMENT_TEST),
            $this->setPassword($data, ResursBank::ENVIRONMENT_PRODUCTION),
            $this->setDebug($data),
        ]);
    }

    /**
     * @param array $data
     * @return string
     */
    private function setFlow(
        array $data
    ): string {
        $error = '';

        try {
            if (isset($data['api']['flow'])) {
                $this->config->setFlow($data['api']['flow']);
            }
        } catch (Exception $e) {
            $this->logger->exception($e);

            $error = $this->module->l('Failed to update API flow. Check the core log for the actual Exception.');
        }

        return $error;
    }

    /**
     * @param array $data
     * @return string
     */
    private function setEnvironment(
        array $data
    ): string {
        $error = '';

        try {
            if (isset($data['api']['environment'])) {
                $this->config->setEnvironment(
                    (int) $data['api']['environment']
                );
            }
        } catch (Exception $e) {
            $this->logger->exception($e);

            $error = $this->module->l('Failed to update API environment. Check the core log for the actual Exception.');
        }

        return $error;
    }

    /**
     * @param array $data
     * @param int $environment
     * @return string
     */
    private function setUsername(
        array $data,
        int $environment
    ): string {
        $error = '';

        try {
            if (isset($data['api']["username_{$environment}"])) {
                $this->config->setUsername(
                    $data['api']["username_{$environment}"],
                    $environment
                );
            }
        } catch (Exception $e) {
            $this->logger->exception($e);

            $error = $this->module->l('Failed to update API username for environment') . ' ' . $environment;
        }

        return $error;
    }

    /**
     * @param array $data
     * @param int $environment
     * @return string
     */
    private function setPassword(
        array $data,
        int $environment
    ): string {
        $error = '';

        try {
            if (
                isset($data['api']["password_{$environment}"]) &&
                (string) $data['api']["password_{$environment}"] !== ''
            ) {
                $this->config->setPassword(
                    $data['api']["password_{$environment}"],
                    $environment
                );
            }
        } catch (Exception $e) {
            $this->logger->exception($e);

            $error = $this->module->l('Failed to update API password for environment') . ' ' . $environment;
        }

        return $error;
    }

    /**
     * @param array $data
     * @return string
     */
    private function setDebug(
        array $data
    ): string {
        $error = '';

        try {
            if (isset($data['advanced']['debug'])) {
                $this->config->setIsDebugEnabled(
                    (int) $data['advanced']['debug'] === 1
                );
            }
        } catch (Exception $e) {
            $this->logger->exception($e);

            $error = $this->module->l('Failed to update debug mode.');
        }

        return $error;
    }
}
