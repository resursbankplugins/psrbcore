<?php
/**
 * Copyright © Resurs Bank AB. All rights reserved.
 * See LICENSE for license details.
 */

declare(strict_types=1);

namespace Resursbank\Core\Logger;

use Exception;

/**
 * Contract for log handlers.
 * @todo Both Magento and Prestashop include PSR, maybe we could use that logger instead of our own? It should lessen our footprint a bit.
 */
interface LoggerInterface
{
    /**
     * @param string $text
     * @param bool $force
     *
     * @return self
     */
    public function info(string $text, bool $force = false): self;

    /**
     * @param string $text
     * @param bool $force
     *
     * @return self
     */
    public function error(string $text, bool $force = false): self;

    /**
     * @param Exception $error
     * @param bool $force
     *
     * @return self
     */
    public function exception(Exception $error, bool $force = false): self;

    /**
     * @return bool
     */
    public function isEnabled(): bool;
}
