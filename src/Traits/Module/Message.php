<?php
/**
 * Copyright © Resurs Bank AB. All rights reserved.
 * See LICENSE for license details.
 */

declare(strict_types=1);

namespace Resursbank\Core\Traits\Module;

use Module as PrestaModule;
use PrestaShop\PrestaShop\Core\Exception\ContainerNotFoundException;
use RuntimeException;
use Symfony\Component\HttpFoundation\Session\Flash\FlashBagInterface;

/**
 * Methods to interact with message bag.
 */
trait Message
{
    /**
     * @param PrestaModule $module
     *
     * @return FlashBagInterface
     *
     * @throws ContainerNotFoundException
     */
    public function getMessageBag(
        PrestaModule $module
    ): FlashBagInterface {
        if (!$module->getContainer()->has('session')) {
            throw new RuntimeException('No session declaration in container.');
        }

        $bag = $module->getContainer()->get('session')->getFlashBag();

        if (!$bag instanceof FlashBagInterface) {
            throw new RuntimeException('Invalid message bag.');
        }

        return $bag;
    }

    /**
     * @param PrestaModule $module
     * @param string $message
     *
     * @return void
     *
     * @throws ContainerNotFoundException
     */
    public function addErrorMessage(
        PrestaModule $module,
        string $message
    ): void {
        $this->getMessageBag($module)->add('error', $message);
    }
}
