<?php
/**
 * Copyright © Resurs Bank AB. All rights reserved.
 * See LICENSE for license details.
 */

declare(strict_types=1);

namespace Resursbank\Core\Traits\Module;

use Exception;
use PrestaShop\PrestaShop\Core\Addon\Module\ModuleManager;
use PrestaShop\PrestaShop\Core\Exception\ContainerNotFoundException;

/**
 * Resurs Bank module validation methods, such as to check whether modules are
 * installed and enabled.
 */
trait Validate
{
    /**
     * @return ModuleManager
     * @throws Exception
     */
    public function getModuleManager(): ModuleManager
    {
        $moduleManager = $this->get('prestashop.module.manager');

        if (!$moduleManager instanceof ModuleManager) {
            throw new Exception('Failed to find module manager in container.');
        }

        return $moduleManager;
    }

    /**
     * Name - avoid collision with \Module::isInstalled()
     *
     * @throws ContainerNotFoundException
     * @throws Exception
     */
    public function isModuleInstalled(string $module): bool
    {
        return $this->getModuleManager()->isInstalled($module);
    }

    /**
     * Name - avoid collision with \Module::isEnabled()
     *
     * @throws ContainerNotFoundException
     * @throws Exception
     */
    public function isModuleEnabled(string $module): bool
    {
        return $this->getModuleManager()->isEnabled($module);
    }

    /**
     * @throws ContainerNotFoundException
     */
    public function isAvailable(string $module): bool
    {
        return (
            $this->isModuleInstalled($module) &&
            $this->isModuleEnabled($module)
        );
    }

    /**
     * Validate that Resurs Bank Core module is installed and enabled.
     *
     * @return void
     * @throws ContainerNotFoundException
     * @throws Exception
     */
    public function validateCore(): void
    {
        if (!$this->isAvailable('psrbcore')) {
            throw new Exception('Core module is not installed / enabled.');
        }
    }
}
