<?php
/**
 * Copyright © Resurs Bank AB. All rights reserved.
 * See LICENSE for license details.
 */

declare(strict_types=1);

namespace Resursbank\Core\Traits\Module;

use Module as PrestaModule;
use ReflectionException;
use Resursbank\Core\Exception\MissingPathException;
use Resursbank\Core\Service\Filesystem;
use TorneLIB\Exception\ExceptionHandler;
use TorneLIB\Utils\Generic;

/**
 * Supplies method to execute common initialization logic.
 */
trait Init
{
    /**
     * @throws ReflectionException
     * @throws MissingPathException
     * @throws ExceptionHandler
     */
    public function init(
        PrestaModule $module,
        int $needInstance = 0
    ): void {
        // Using dependencies via ECom for extracting data from composer.json.
        $generic = new Generic();

        // Path to module main class.
        $path = Filesystem::getPath(get_class($module), 'composer.json');

        // Module description from composer.json
        $description = $generic->getComposerTag($path, 'description');

        // Basic module information.
        $module->name = $generic->getComposerShortName($path);
        $module->version = $generic->getVersionByAny($path);
        $module->author = 'Resurs Bank';
        $module->need_instance = $needInstance > 0 ? 1 : 0;
        $module->bootstrap = true;
        $module->ps_versions_compliancy = [
            'min' => '1.7.7.0',
            'max' => '1.7.9.99',
        ];
        $module->displayName = $description;
        $module->description = $description;

        // Section of module manager / catalog in backoffice.
        $module->tab = 'payments_gateways';
    }
}
