<?php
/**
 * Copyright © Resurs Bank AB. All rights reserved.
 * See LICENSE for license details.
 */

declare(strict_types=1);

namespace Resursbank\Core\Traits\Module;

use Configuration;
use Exception;
use Language;
use OrderState as PsOrderState;
use Resursbank\Core\ModuleInterface;
use Resursbank\Core\Service\Filesystem;

/**
 * Contains methods to handle order states.
 */
trait OrderState
{
    /**
     * @param ModuleInterface $module
     * @param array $states
     *
     * @throws Exception
     */
    public function installOrderStates(
        ModuleInterface $module,
        array $states
    ): void {
        foreach ($states as $code => $data) {
            // Only create an order state if it doesn't already exist.
            if ((bool) Configuration::get($code) === false) {
                $state = $this->createOrderState($data);

                /* Store in config which id belongs to the state code
                   (so we can later retrieve it easily). */
                Configuration::updateValue($code, $state->id);

                // Install logotype.
                $this->installOrderStateLogo($module, $state);
            }
        }
    }

    /**
     * @param array $data
     *
     * @return PsOrderState
     *
     * @throws Exception
     */
    public function createOrderState(array $data): PsOrderState
    {
        // Create order state.
        $orderState = new PsOrderState();
        $names = [];

        foreach ($data as $key => $val) {
            $orderState->{$key} = $val;
        }

        foreach (Language::getLanguages(false) as $language) {
            $names[$language['id_lang']] = $data['name'];
        }

        $orderState->name = $names;
        $orderState->save();

        if (!isset($orderState->id) || (int) $orderState->id === 0) {
            throw new Exception(
                'Failed to create order state using data ' .
                json_encode($data, JSON_THROW_ON_ERROR)
            );
        }

        return $orderState;
    }

    /**
     * @param ModuleInterface $module
     * @param PsOrderState $state
     *
     * @throws Exception
     */
    public function installOrderStateLogo(
        ModuleInterface $module,
        PsOrderState $state
    ): void {
        if (!isset($state->id) || (int) $state->id === 0) {
            throw new Exception(
                'Cannot install logotype for order state without an id.'
            );
        }

        // Get logotype src (using the same logotype for all order states atm).
        $source = Filesystem::getImg(get_class($module), 'orderstates_logo.gif');

        // Get logotype destination directory.
        $destinationDirectory = _PS_ORDER_STATE_IMG_DIR_;

        if (!is_dir($destinationDirectory)) {
            throw new Exception(
                'Order state logotype destination directory does not exist ' .
                $destinationDirectory
            );
        }

        // Get complete file destination path.
        $destination = ($destinationDirectory . '/' . $state->id . '.gif');

        // Copy the logotype.
        if (!copy($source, $destination)) {
            throw new Exception('Failed to copy logotype for ' . $state->id);
        }
    }
}
