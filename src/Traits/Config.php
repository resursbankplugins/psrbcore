<?php
/**
 * Copyright © Resurs Bank AB. All rights reserved.
 * See LICENSE for license details.
 */

declare(strict_types=1);

namespace Resursbank\Core\Traits;

use InvalidArgumentException;
use Resursbank\RBEcomPHP\ResursBank;

/**
 * Common configuration related methods relevant for all modules.
 */
trait Config
{
    /**
     * Returns configuration name.
     *
     * @param string $key
     *
     * @return string
     */
    public function getId(
        string $key
    ): string {
        if ($key === '') {
            throw new InvalidArgumentException(
                'Please supply configuration value key.'
            );
        }

        return strtoupper("RESURSBANK_${key}");
    }

    /**
     * @param int $environment
     *
     * @throws InvalidArgumentException
     *
     * @todo Same validation occurs in API credentials, we should centralize this.
     */
    public function validateEnvironment(
        int $environment
    ): void {
        if (
            $environment !== ResursBank::ENVIRONMENT_TEST &&
            $environment !== ResursBank::ENVIRONMENT_PRODUCTION
        ) {
            throw new InvalidArgumentException('Invalid API environment.');
        }
    }
}
