<?php
/**
 * Copyright © Resurs Bank AB. All rights reserved.
 * See LICENSE for license details.
 */

declare(strict_types=1);

namespace Resursbank\Core\Crypto;

use Defuse\Crypto\Crypto;
use Defuse\Crypto\Exception\CryptoException;
use Defuse\Crypto\Exception\EnvironmentIsBrokenException;
use Defuse\Crypto\Exception\WrongKeyOrModifiedCiphertextException;

/**
 * Encryption library using OpenSSL.
 */
class Openssl
{
    /**
     * @var string
     */
    private $key;

    /**
     * @param string $secret
     */
    public function __construct(
        string $secret
    ) {
        $this->key = $secret;
    }

    /**
     * @param string $data
     * @return string
     * @throws CryptoException
     * @throws EnvironmentIsBrokenException
     */
    public function encrypt(
        string $data
    ): string {
        return Crypto::encryptWithPassword($data, $this->key);
    }

    /**
     * @param string $data
     * @return string
     * @throws CryptoException
     * @throws EnvironmentIsBrokenException
     * @throws WrongKeyOrModifiedCiphertextException
     */
    public function decrypt(
        string $data
    ): string {
        return Crypto::decryptWithPassword($data, $this->key);
    }
}
