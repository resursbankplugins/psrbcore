<?php
/**
 * Copyright © Resurs Bank AB. All rights reserved.
 * See LICENSE for license details.
 */

namespace Resursbank\Core\Event;

use Symfony\Component\HttpKernel\Event\GetResponseEvent;

/**
 * Start our autoloader at the start of the request. Note that this is required
 * for our services to function properly, otherwise Symfony won't be able to
 * find the classes specified within our YAML files. Please note that this is
 * required alongside the hook defined in the main class, otherwise our
 * autoloader won't always be available.
 */
class RequestListener
{
    /**
     * @param GetResponseEvent $event
     * @return void
     */
    public function onKernelRequest(GetResponseEvent $event)
    {
        require_once(_PS_MODULE_DIR_ . "/psrbcore/src/Service/Autoloader.php");
    }
}
