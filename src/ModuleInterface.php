<?php
/**
 * Copyright © Resurs Bank AB. All rights reserved.
 * See LICENSE for license details.
 */

declare(strict_types=1);

namespace Resursbank\Core;

/**
 * Contract for Resurs Bank modules.
 */
interface ModuleInterface
{
    /**
     * @return string
     */
    public function getVersion(): string;

    /**
     * @return string
     */
    public function getName(): string;
}
