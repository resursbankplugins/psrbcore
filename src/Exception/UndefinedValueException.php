<?php
/**
 * Copyright © Resurs Bank AB. All rights reserved.
 * See LICENSE for license details.
 */

declare(strict_types=1);

namespace Resursbank\Core\Exception;

use Exception;

/**
 * Indicates an undefined value.
 */
class UndefinedValueException extends Exception
{
}
