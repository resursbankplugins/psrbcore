<?php
/**
 * Copyright © Resurs Bank AB. All rights reserved.
 * See LICENSE for license details.
 */

declare(strict_types=1);

use PrestaShop\PrestaShop\Adapter\Entity\ModuleFrontController;
use PrestaShop\PrestaShop\Adapter\Entity\Tools;
use Resursbank\Core\Api\Credentials;
use Resursbank\Core\Entity\ResursbankPaymentMethod;
use Resursbank\Core\Entity\ResursbankPaymentMethod as Method;
use Resursbank\Core\Service\PaymentMethods;

/**
 * Get "Read More" HTML for part payment / payment method info modal.
 *
 * @suppress PhpUnused
 */
class psrbcoreReadmoreModuleFrontController extends ModuleFrontController
{
    /**
     * @throws Exception
     *
     * @see FrontController::postProcess()
     * @since 1.0.0
     * @noinspection PhpMissingParentCallCommonInspection
     */
    public function postProcess(): void
    {
        header('Content-Type: application/json');
        try {
            $html = $this->getHtml();
            http_response_code(200);
            $this->ajaxRender(json_encode([
                'success' => true,
                'message' => '',
                'html' => $html,
                'code' => 200,
            ], JSON_THROW_ON_ERROR));
        } catch (Exception $e) {
            http_response_code(500);
            $this->ajaxRender(json_encode([
                'success' => false,
                'message' => $e->getMessage(),
                'code' => 500,
            ], JSON_THROW_ON_ERROR));
        }
    }

    /**
     * Retrieve price to base API queries on.
     *
     * @return int
     */
    private function getPrice(): int
    {
        return (int) ceil((float) Tools::getValue('price'));
    }

    /**
     * Retrieve payment method to base API queries on.
     *
     * @return Method
     *
     * @throws Exception
     */
    protected function getMethod(): Method
    {
        /** @var PaymentMethods $paymentMethodsService */
        $paymentMethodsService = $this->get('resursbank.core.payment.methods');

        return $paymentMethodsService->getMethod(Tools::getValue('code'));
    }

    /**
     * Retrieve payment method identifier for API call to acquire HTML. Please
     * note that an empty string would make ECom fetch HTML for all available
     * payment methods (using the credentials supplied with initializing the
     * connection instance), which is desirable for DK.
     *
     * @param ResursbankPaymentMethod $method
     *
     * @return string
     */
    private function getIdentifier(
        ResursbankPaymentMethod $method
    ): string {
        return $method->getSpecificCountry() !== 'DK' ?
            (string) $method->getIdentifier() :
            '';
    }

    /**
     * @return string
     *
     * @throws Exception
     */
    public function getHtml(): string
    {
        /** @var Credentials $credentialsApi */
        $credentialsApi = $this->get('resursbank.core.credentials');

        /** @var Credentials $credentialsApi */
        $connectionsApi = $this->get('resursbank.core.api.connection');

        $connection = $connectionsApi->getConnection(
            $credentialsApi->get()
        );

        return $connection->getCostOfPriceInformation(
            $this->getIdentifier($this->getMethod()),
            $this->getPrice(),
            false,
            true
        );
    }
}
