/**
 * Copyright © Resurs Bank AB. All rights reserved.
 * See LICENSE for license details.
 */

/**
 * @typedef {object} RbcView.ReadMoreFnParams
 * @property {number} price
 * @property {string} methodCode
 * @property {string} remodalId,
 * @property {string} contentId,
 * @property {string} loaderId,
 * @property {string|undefined} url,
 */

/**
 * @typedef {object} RbcView.ReadMoreInstance
 * @property {function():boolean} canFetchData
 * @property {function():Deferred} fetchData
 * @property {jQuery} contentEl
 * @property {jQuery} loaderEl
 * @property {jQuery} readMoreEl
 * @property {*} remodalInstance
 */

/**
 * @callback ReadMoreFn
 * @param {RbcView.ReadMoreFnParams} data
 * @return {RbcView.ReadMoreInstance}
 * @memberOf RbcView
 */

/**
 * @param {RbcView.ReadMoreFnParams} data
 * @returns {RbcView.ReadMoreInstance}
 * @constructor
 */
window.psrbcore.View.ReadMore = function (
    data
) {
    /**
     * @type {RbcModel.ReadMore}
     */
    var model = window.psrbcore.Model.ReadMore;

    /**
     * @type {RbcLib.ReadMore}
     */
    var lib = window.psrbcore.Lib.ReadMore;

    /**
     * Information on part payment for the product. This is shown in the
     * modal window.
     *
     * @type {jQuery}
     */
    var contentEl = $('#' + data.contentId);

    /**
     * The loader of the modal window to show when data is being fetched.
     *
     * @type {jQuery}
     */
    var loaderEl = $('#' + data.loaderId);

    /**
     * The link that will open the modal window.
     *
     * @type {jQuery}
     */
    var readMoreEl = $('#' + data.remodalId + '-psrbcore-link-read-more');

    /**
     * HTML string.
     *
     * @type {string}
     */
    var content = '';

    /**
     * @type {jQuery}
     */
    var remodalInstance = lib.getRemodalInstance(data.remodalId)
        .remodal({
            hashTracking: false
        });

    /**
     * Whether data can be fetched.
     *
     * @returns {boolean}
     */
    function canFetchData() {
        return !model.isFetchingData() &&
            content === '';
    }

    /**
     * Fetches part payment data and displays it in the modal.
     *
     * @returns {Deferred}
     */
    function fetchData() {
        contentEl.hide();
        loaderEl.show();

        model.isFetchingData(true);

        var request = lib.getCostOfPurchase(
            data.price,
            data.methodCode,
            data.url
        );

        request.done(function (response) {
            loaderEl.fadeOut(1000, function () {
                if (response.hasOwnProperty('html')) {
                    content = response.html;
                    contentEl.html(response.html);
                } else if (response.hasOwnProperty('error')) {
                    contentEl.html(response.error);
                } else {
                    contentEl.html(
                        'An unknown error occurred.'
                    );
                }

                $(contentEl).fadeIn(1000);
            });
        });

        request.fail(function (error) {
            loaderEl.fadeOut(1000, function () {
                contentEl.html(error);

                $(contentEl).fadeIn(1000);
            });
        });

        request.always(function () {
            model.isFetchingData(false);
        });

        return request;
    }

    if (contentEl.length > 0 &&
        loaderEl.length > 0 &&
        readMoreEl.length > 0 &&
        data.price > 0
    ) {
        readMoreEl.on('click', function () {
            if (canFetchData()) {
                fetchData();
            }
        });
    }

    return {
        fetchData: fetchData,
        contentEl: contentEl,
        loaderEl: loaderEl,
        readMoreEl: readMoreEl,
        remodalInstance: remodalInstance
    };
};
