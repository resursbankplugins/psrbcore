/**
 * Copyright © Resurs Bank AB. All rights reserved.
 * See LICENSE for license details.
 */

/**
 * @type {RbcModel.ReadMore}
 */
window.psrbcore.Model.ReadMore = (
    /**
     * @return {RbcModel.ReadMore}
     */
    function () {
        'use strict';

        /**
         * @constant
         * @type {object}
         */
        var PRIVATE = {
            /**
             * @type {boolean}
             */
            isFetchingData: false
        };

        /**
         * @namespace RbcModel.ReadMore
         * @readonly
         * @constant
         */
        var EXPORT = {
            /**
             * @param {boolean} [value]
             * @returns {boolean}
             */
            isFetchingData: function (value) {
                if (typeof value === 'boolean') {
                    PRIVATE.isFetchingData = value;
                }

                return PRIVATE.isFetchingData;
            }
        };

        return Object.freeze(EXPORT);
    }()
);
