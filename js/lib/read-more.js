/**
 * Copyright © Resurs Bank AB. All rights reserved.
 * See LICENSE for license details.
 */

// phpcs:ignoreFile
window.psrbcore.Lib.ReadMore = (
    /**
     * @param {jQuery} $
     * @param {RbcModel.ReadMore} Model
     * @param {RbcLib.Config} Config
     * @returns {RbcLib.ReadMore}
     */
    function (
        $,
        Model,
        Config
    ) {
        'use strict';

        /**
         * @typedef {object} RbcLib.ReadMore.Call
         * @property {string} type
         * @property {string} dataType
         * @property {string} url
         * @property {object} data
         * @property {string} data.code
         * @property {number} data.price
         * @property {boolean} data.ajax
         */

        /**
         * @constant
         * @namespace RbcLib.ReadMore
         */
        var EXPORT = {
            /**
             * Creates and executes a call to the server that will fetch a table
             * of part payment information about a given price and payment method.
             * The table is returned as an HTML string.
             *
             * @param {number} price - Float.
             * @param {string} [methodCode]
             * @param {string} [url]
             * @returns {Deferred}
             */
            getCostOfPurchase: function (price, methodCode, url) {
                var deferred = $.Deferred();
                var call = $.ajax(
                    EXPORT.getCostOfPurchaseCall(price, methodCode, url)
                );

                call.done(function (response) {
                    deferred.resolve(response);
                });

                call.fail(function () {
                    deferred.reject('No information could be found.');
                });

                call.always(function () {
                    Model.isFetchingData(false);
                });

                return deferred;
            },

            /**
             * Produces a call object to make a request to fetch part payment
             * data.
             *
             * @param {number} price - Float.
             * @param {string} [methodCode]
             * @param {string} [url]
             * @returns {RbcLib.ReadMore.Call}
             */
            getCostOfPurchaseCall: function (price, methodCode, url) {
                return {
                    type: 'POST',
                    dataType: 'json',
                    url: (typeof url === 'string' && url !== '') ?
                        url :
                        Config.getCostOfPurchaseUrl(),
                    data: {
                        ajax: true,
                        code: typeof methodCode === 'string' ? methodCode : '',
                        price: price
                    }
                };
            },

            /**
             * Retrieves the element that will be used to initialize the
             * Remodal instance from the DOM.
             *
             * @param {string} remodalId
             * @returns {jQuery}
             */
            getRemodalInstance: function (remodalId) {
                return $('[data-remodal-id="' + remodalId + '"]');
            }
        };

        return Object.freeze(EXPORT);
    }(
        $,
        window.psrbcore.Model.ReadMore,
        window.psrbcore.Lib.Config
    )
);

