/**
 * Copyright © Resurs Bank AB. All rights reserved.
 * See LICENSE for license details.
 */

/**
 * @type {RbcLib.Config}
 */
window.psrbcore.Lib.Config = (
    function () {
        'use strict';
    
        /**
         * @namespace RbcLib.Config
         * @readonly
         * @constant
         */
        var EXPORT = {
            /**
             * Returns the URL to contact the controller responsible for
             * fetching part payment information.
             *
             * @returns {string} An empty string will be returned if the URL
             * couldn't be found.
             */
            getCostOfPurchaseUrl: function() {
                return window.hasOwnProperty('psrbcore') &&
                    typeof window.psrbcore.getCostOfPurchaseUrl === 'string' ?
                        window.psrbcore.getCostOfPurchaseUrl :
                        '';
            }
        };
    
        return Object.freeze(EXPORT);
    }()
);
