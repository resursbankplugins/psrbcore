
# Resurs Bank - PrestaShop module - Core

## Description

Core part of PrestaShop module.

---

## Prerequisites

* [PrestaShop 1.7.7+]

---

## Changelog

#### 1.0.0

* Initial release.
