<?php

global $_MODULE;
$_MODULE = array();
$_MODULE['<{psrbcore}prestashop>psrbcore_5f96a0ad69d872f5bac0d00c6933cc5a'] = 'Kunde inte uppdatera API-flödet. Kontrollera core-loggen för det faktiska felet.';
$_MODULE['<{psrbcore}prestashop>psrbcore_ae1f6ab4627a628d69e59fe6a4a39bcc'] = 'Kunde inte ändra API-miljö. Kontrollera core-loggen för det faktiska felet.';
$_MODULE['<{psrbcore}prestashop>psrbcore_948c9d068e3ea97ac00d3149051e139a'] = 'Kunde inte uppdatera API-användarnamn for miljön. Kontrollera core-loggen för det faktiska felet.';
$_MODULE['<{psrbcore}prestashop>psrbcore_c95bfadbf60ec7720abacd198bea0c32'] = 'Kunde inte uppdatera API-lösenord for miljön. Kontrollera core-loggen för det faktiska felet.';
$_MODULE['<{psrbcore}prestashop>psrbcore_cfe69a59c3c493b1401513816710b4ad'] = 'Kunde inte uppdatera debugläge. Kontrollera core-loggen för det faktiska felet.';
$_MODULE['<{psrbcore}prestashop>psrbcore_756d97bb256b8580d4d71ee0c547804e'] = 'Produktion';
$_MODULE['<{psrbcore}prestashop>psrbcore_decbe415d8accca7c2c16d48a79ee934'] = 'Läs mer';
$_MODULE['<{psrbcore}prestashop>psrbcore_b4a34d3f58b039e7685c2e39b5413757'] = 'Lyckad uppdatering.';
$_MODULE['<{psrbcore}prestashop>psrbcore_8b339ac136d17ecd3f1f18e20613c3ba'] = 'Datasynkronisering lyckades.';
$_MODULE['<{psrbcore}prestashop>psrbcore_ab69619525c90e6ff69245f0d93a7d17'] = 'Datasynkronisering misslyckades.';

