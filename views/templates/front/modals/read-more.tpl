<!--
/**
 * Copyright © Resurs Bank AB. All rights reserved.
 * See LICENSE for license details.
 */
-->

<div class="psrbcore-read-more" data-read-more-id="{$remodalId}">
    {if $logo !== ''}
        <div>
            <img class="psrbcore-read-more-logo" src="{$logo}" alt=""/>
        </div>
    {/if}
    <div class="psrbcore-info-container">
        {if $label !== ''}
            <div>{$label}</div>
        {/if}
        {if $info !== ''}
          {if $info !== '' && $label !== ''}
              <br>
          {/if}
          <div>{$info}</div>
        {/if}
        <div class="psrbcore-read-more">
            <a
                style="cursor:pointer; font-weight:bold;"
                id="{$remodalId|cat:'-psrbcore-link-read-more'}"
                data-remodal-target="{$remodalId}"
            >
                {$linkTitle}
            </a>
        </div>
    </div>
</div>
<div class="remodal-bg">
    <div
        class="remodal psrbcore-modal"
        data-remodal-id="{$remodalId}">
        <button
                data-remodal-id="{$remodalId|cat:'-close-btn'}"
                data-remodal-action="close"
                class="remodal-close">
        </button>
        {if $modalTitle !== ''}
            <h1>{$modalTitle}</h1>
        {/if}
        <div>
            <div
                class="psrbcore-modal-loader"
                id="{$loaderId}">
            </div>
            <div id="{$contentId}"></div>
        </div>
    </div>
</div>
