<?php
/**
 * Copyright © Resurs Bank AB. All rights reserved.
 * See LICENSE for license details.
 */

declare(strict_types=1);

/* Start our custom atuoloader. We must start it at this point for the module
 manager to function. */
require_once(_PS_MODULE_DIR_ . "/psrbcore/src/Service/Autoloader.php");

use Doctrine\ORM\EntityManagerInterface;
use PrestaShop\PrestaShop\Adapter\Module\Module as ModuleAdapter;
use PrestaShop\PrestaShop\Core\Addon\AddonListFilter;
use PrestaShop\PrestaShop\Core\Addon\AddonListFilterType;
use PrestaShop\PrestaShop\Core\Addon\AddonRepositoryInterface;
use PrestaShop\PrestaShop\Core\Domain\Shop\ValueObject\ShopConstraint;
use Resursbank\Core\Entity\ResursbankOrder;
use Resursbank\Core\Exception\InstallerException;
use Resursbank\Core\Exception\MissingPathException;
use Resursbank\Core\Logger\Logger;
use Resursbank\Core\Model\OrderStates;
use Resursbank\Core\ModuleInterface;
use Resursbank\Core\Repository\ResursbankOrderRepository;
use Resursbank\Core\Service\Sql;
use Resursbank\Core\Service\Tab as ResursTab;
use Resursbank\Core\Traits\Module\Init;
use Resursbank\Core\Traits\Module\OrderState;
use Resursbank\Core\Traits\Module\Validate;
use Resursbank\RBEcomPHP\ResursBank;
use TorneLIB\Exception\ExceptionHandler;

// Make sure the file is loaded within the context of PrestaShop.
if (!defined('_PS_VERSION_')) {
    exit;
}

/**
 * Resurs Bank Core module.
 *
 * This module contains basic functionality shared between all modules.
 *
 * NOTE: This naming scheme is required to ensure a unique module name which
 * follows Presta Shops advice to name a module (a-z0-9 only).
 *
 * @noinspection PhpUnused
 */
class psrbcore extends Module implements ModuleInterface
{
    use Init;
    use Validate;
    use OrderState;

    /**
     * List of hooks (event observers) to register.
     *
     * @var string[]
     */
    private $hooks = [
        'actionFrontControllerSetMedia',
        'actionDispatcherBefore',
        'actionDispatcherAfter'
    ];

    /**
     * @throws ReflectionException
     * @throws ExceptionHandler
     * @throws MissingPathException
     */
    public function __construct()
    {
        $this->init($this);
        parent::__construct();
    }

    /**
     * @return bool
     *
     * @throws InstallerException
     * @throws PrestaShopDatabaseException
     * @throws PrestaShopException
     *
     * @todo Hook for other installers.
     */
    public function install(): bool
    {
        /* @noinspection BadExceptionsProcessingInspection */
        try {
            if (!parent::install()) {
                throw new Exception('Parent installation procedure failed.');
            }

            // Install tables.
            Sql::exec(self::class, 'install.sql');

            // Configs not loaded yet, cannot access class through container.
            $tab = ResursTab::getInstance($this);

            /*
             * Main tab, displayed under "Shop Parameters". This is just a link
             * really, but it's required by PrestaShop to add it this way.
             *
             * Link to Resurs Bank configuration page.
             */
            $tab->create(
                $this->getName(),
                'ResursBank',
                'ShopParameters',
                'Resurs Bank',
                'resursbank_admin_config',
                'Modules.ResursBank.Core.Tab'
            );

            /*
             * Core configuration page tab. This will be displayed after you
             * click on the main tab (created using the statement above).
             *
             * Core configuration view.
             */
            $tab->create(
                $this->getName(),
                'ResursBankCore',
                'ResursBank',
                'Core',
                'resursbank_admin_config',
                'Modules.ResursBank.Core.Tab'
            );

            // Install custom order states.
            $this->installHooks();
            $this->installOrderStates($this, OrderStates::states());

            // Run installation procedure of each submodule.
            foreach ($this->getResursBankModules() as $module) {
                /* @noinspection NotOptimalIfConditionsInspection */
                if (!$this->isModuleInstalled($module->getName()) &&
                    !$module->install()
                ) {
                    throw new Exception('Failed to install ' . $module->getName());
                }
            }
        } catch (Exception $e) {
            $this->uninstall();
            throw new InstallerException($e->getMessage());
        }

        return true;
    }

    /**
     * @return bool
     *
     * @throws InstallerException
     */
    public function uninstall(): bool
    {
        try {
            // Run uninstallation procedure of each submodule.
            foreach ($this->getResursBankModules() as $mod) {
                /* @noinspection NotOptimalIfConditionsInspection */
                if ($this->isModuleInstalled($mod->getName()) &&
                    !$mod->uninstall()
                ) {
                    throw new Exception('Failed to remove ' . $mod->getName());
                }
            }

            if (!parent::uninstall()) {
                throw new Exception('Uninstall failed.');
            }

            // Configs not loaded yet, cannot access class through container.
            $tab = ResursTab::getInstance($this);
            $tab->delete('ResursBankCore');
            $tab->delete('ResursBank');
        } catch (Exception $e) {
            throw new InstallerException($e->getMessage());
        }

        return true;
    }

    /**
     * @param bool $force_all
     *
     * @return bool
     *
     * @throws InstallerException
     */
    public function enable($force_all = false): bool
    {
        try {
            if (!parent::enable($force_all)) {
                throw new Exception('Parent enable procedure failed.');
            }

            // Run enable procedure of each submodule.
            foreach ($this->getResursBankModules() as $module) {
                /* @noinspection NotOptimalIfConditionsInspection */
                if ($this->isModuleInstalled($module->getName()) &&
                    !$this->isModuleEnabled($module->getName()) &&
                    !$module->enable()
                ) {
                    throw new InstallerException(
                        'Failed to enable submodule ' . $module->getName()
                    );
                }
            }

            // Configs not loaded yet, cannot access class through container.
            $tab = ResursTab::getInstance($this);
            $tab->activate('ResursBankCore');
            $tab->activate('ResursBank');

            $this->registerHook($this->hooks);
        } catch (Exception $e) {
            throw new InstallerException($e->getMessage());
        }

        return true;
    }

    /**
     * @param bool $force_all
     *
     * @return bool
     *
     * @throws InstallerException
     */
    public function disable($force_all = false): bool
    {
        try {
            if (!parent::disable($force_all)) {
                throw new Exception('Parent disable procedure failed.');
            }

            // Run enable procedure of each submodule.
            foreach ($this->getResursBankModules() as $module) {
                /* @noinspection NotOptimalIfConditionsInspection */
                if ($this->isAvailable($module->getName()) &&
                    !$module->disable()
                ) {
                    throw new Exception(
                        'Failed to disable submodule ' . $module->getName()
                    );
                }
            }

            // Configs not loaded yet, cannot access class through container.
            $tab = ResursTab::getInstance($this);
            $tab->deactivate('ResursBankCore');
            $tab->deactivate('ResursBank');

            foreach ($this->hooks as $hook) {
                $this->unregisterHook($hook);
            }
        } catch (Exception $e) {
            throw new InstallerException($e->getMessage());
        }

        return true;
    }

    /**
     * @return void
     *
     * @throws PrestaShopDatabaseException
     * @throws PrestaShopException
     */
    public function installHooks(): void
    {
        if (Hook::getIdByName('resursBankAfterPaymentMethodSync') === false) {
            $hook = new Hook();
            $hook->name = 'resursBankAfterPaymentMethodSync';
            $hook->title = 'Sync data from Resurs Bank';
            $hook->description = 'Execute custom business logic when we sync ' .
                'data from the API.';
            $hook->position = 1;
            $hook->add();
        }
    }

    /**
     * This method will search for all Resurs Bank related modules. We utilise
     * this to execute the installation / uninstallation procedure of each
     * submodule we locate when this module is installed (in an attempt to make
     * the installation procedure simpler as the client otherwise would need to
     * locate and install / uninstall each module within the module manager
     * interface). Since the submodules are independent, and we can never be
     * sure which are installed, we have to search for them.
     *
     * We cannot use the after installation hook, because hooks aren't
     * registered until after a module has been installed (i.e. the after
     * installation hook cannot be used within a submodules install() method,
     * since that method is required to be executed before any hooks can fire
     * at all within the module).
     *
     * We cannot use Symfony's Event Dispatcher for the same reason as above.
     *
     * NOTE: This module is filtered from the results.
     *
     * @return array<ModuleInterface>
     *
     * @throws Exception
     */
    private function getResursBankModules(): array
    {
        $result = [];

        $addons = $this->get('prestashop.core.admin.module.repository');

        if (!$addons instanceof AddonRepositoryInterface) {
            throw new Exception('Failed to obtain addon repository.');
        }

        $filters = (new AddonListFilter())
            ->setType(AddonListFilterType::MODULE);

        foreach ($addons->getFilteredList($filters) as $name => $module) {
            if ($this->validateResursBankModule($module)) {
                $this->loadModuleClass($name);

                $result[] = new $name();
            }
        }

        return $result;
    }

    /**
     * Make sure that a Module instance infers module stemming from Resurs Bank.
     *
     * NOTE: We utilised class_implements to confirm the module implemented our
     * own ModuleInterface for additional safety, however this caused problems
     * on some installations and wasn't strictly speaking necessary. If added
     * back it should be tested thoroughly before released.
     *
     * @param ModuleAdapter $module
     *
     * @return bool
     */
    private function validateResursBankModule(
        ModuleAdapter $module
    ): bool {
        $name = $module->get('name');

        return
            $name !== self::class &&
            strncmp($name, 'psrb', 4) === 0
            ;
    }

    /**
     * @return string
     */
    public function getVersion(): string
    {
        return $this->version;
    }

    /**
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * Declares we intend to use the new interface to translate our module.
     *
     * @return bool
     * @noinspection PhpMissingParentCallCommonInspection
     */
    public function isUsingNewTranslationSystem(): bool
    {
        return true;
    }

    /**
     * @return Logger
     *
     * @throws PrestaShopException
     */
    private function getLog(): Logger
    {
        try {
            $logger = $this->get('resursbank.core.logger');

            if (!$logger instanceof Logger) {
                throw new PrestaShopException('Failed to resolve logger.');
            }
        } catch (Exception $e) {
            throw new PrestaShopException($e->getMessage());
        }

        return $logger;
    }

    /**
     * Add address fetching and phone validation JS too checkout.
     *
     * @return void
     *
     * @throws Exception
     */
    public function hookActionFrontControllerSetMedia(): void
    {
        $self = $this->context->controller->php_self;

        if ($self === 'product' || $self === 'order') {
            Media::addJsDef([
                'psrbcore' => [
                    'getCostOfPurchaseUrl' => $this->getModuleLink('readmore'),
                ],
            ]);

            $this->context->controller->registerJavascript(
                $this->name . '-init',
                $this->getPathUri() . 'js/psrbcore.js',
            );
        }
    }

    /**
     * Start our custom autoloader at the beginning of any request.
     *
     * @return void
     */
    public function hookActionDispatcherBefore(): void
    {
        require_once(_PS_MODULE_DIR_ . "/psrbcore/src/Service/Autoloader.php");
    }

    /**
     * @return void
     * @throws Exception
     */
    public function hookActionDispatcherAfter(): void
    {
        if ($this->context->controller instanceof OrderConfirmationControllerCore) {
            $orderId = $this->context->controller->id_order;

            /** @var EntityManagerInterface $entityManager */
            $entityManager = $this->get('doctrine.orm.entity_manager');

            /** @var ResursbankOrderRepository $orderRepository */
            $orderRepository = $this->get('resursbank.core.order.repository');

            /** @var ResursbankOrder $order */
            $order = $orderRepository->findOneBy(['orderId' => $orderId]);

            if (!is_object($order)) {
                $this->createResursbankOrder($orderId);
            } else {
                $order->setReadyForCallback(true);
                $entityManager->persist($order);
                $entityManager->flush();
            }
        }
    }

    /**
     * Creates a ResursbankOrder object with the callback flag set to true
     *
     * @param int $orderId
     * @return void
     * @throws Exception
     */
    private function createResursbankOrder(int $orderId): void
    {
        $order = new ResursbankOrder();

        /** @var Resursbank\Core\Config\Config $config */
        $config = $this->get('resursbank.core.config');

        $isTest = $config->getEnvironment(
            ShopConstraint::shop((int) $this->context->cart->id_shop)
        ) === ResursBank::ENVIRONMENT_TEST;

        $order->setOrderId($orderId)
            ->setReadyForCallback(true)
            ->setTest($isTest);

        /** @var EntityManagerInterface $entityManager */
        $entityManager = $this->get('doctrine.orm.entity_manager');

        $entityManager->persist($order);
        $entityManager->flush();
    }

    /**
     * @param string $key
     * @param array $params
     *
     * @return string
     *
     * @since 1.0.0
     */
    public function getModuleLink(string $key, array $params = []): string
    {
        return $this->context->link->getModuleLink(
            $this->name,
            $key,
            $params,
            true
        );
    }

    /**
     * @param string $moduleName
     * @param string $pathUri
     * @param FrontController $controller
     * @return void
     */
    public static function addRemodalAssets(
        string $moduleName,
        string $pathUri,
        FrontController $controller
    ): void {
        $coreModulePath = str_replace($moduleName, __CLASS__, $pathUri);

        $controller->registerJavascript(
            $moduleName . '-core-lib-remodal',
            $coreModulePath . 'js/lib/remodal.js',
        );

        $controller->registerStylesheet(
            $moduleName . '-core-css-remodal',
            $coreModulePath . 'assets/css/remodal.css',
        );

        $controller->registerStylesheet(
            $moduleName . '-core-css-remodal-custom',
            $coreModulePath . 'assets/css/remodal-custom.css',
        );

        $controller->registerStylesheet(
            $moduleName . '-core-css-remodal-default-theme',
            $coreModulePath . 'assets/css/remodal-default-theme.css',
        );
    }

    /**
     * @param string $moduleName
     * @param string $pathUri
     * @param FrontController $controller
     * @return void
     */
    public static function addReadMoreAssets(
        string $moduleName,
        string $pathUri,
        FrontController $controller
    ): void {
        $coreModulePath = str_replace($moduleName, __CLASS__, $pathUri);

        $controller->registerJavascript(
            $moduleName . '-core-model-read-more',
            $coreModulePath . 'js/model/read-more.js',
        );

        $controller->registerJavascript(
            $moduleName . '-core-lib-read-more',
            $coreModulePath . 'js/lib/read-more.js',
        );

        $controller->registerJavascript(
            $moduleName . '-core-view-read-more',
            $coreModulePath . 'js/view/read-more.js',
        );

        $controller->registerStylesheet(
            $moduleName . '-core-css-read-more',
            $coreModulePath . 'assets/css/read-more.css',
        );
    }

    /**
     * Without Composer the main class of our submodules won't have loaded,
     * thus we cannot continue.
     *
     * @param string $class
     * @return void
     * @throws Exception
     */
    private function loadModuleClass(string $class): void
    {
        if (!class_exists($class)) {
            $path = _PS_MODULE_DIR_ . "/${class}/${class}.php";

            if (!file_exists($path)) {
                throw new Exception("Missing ${class} main class.");
            }

            require_once($path);
        }
    }
}
